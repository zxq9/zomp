%%% @doc
%%% The Zomp Client Supervisor
%%%
%%% This supervisor maintains the lifecycle of all zomp_client worker processes.
%%% @end

-module(zomp_client_sup).
-vsn("0.6.3").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start_acceptor/1]).
-export([start_link/0]).
-export([init/1]).



%%% Interface Functions

-spec start_acceptor(ListenSocket) -> Result
    when ListenSocket :: inet:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: term().
%% @doc
%% Start a Client connection handler.
%% (Should only be called from zomp_client).

start_acceptor(ListenSocket) ->
    supervisor:start_child(?MODULE, [ListenSocket]).



%%% Startup

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Spawns a single, registered worker process.
%%
%% Error conditions, supervision strategies, and other important issues are
%% explained in the supervisor module docs:
%% http://erlang.org/doc/man/supervisor.html

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% Do not call this function directly -- it is exported only because it is a
%% necessary part of the OTP supervisor behavior.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},

    Client = {zomp_client,
              {zomp_client, start_link, []},
              temporary,
              brutal_kill,
              worker,
              [zomp_client]},

    Children = [Client],
    {ok, {RestartStrategy, Children}}.
