%%% @doc
%%% The Zomp top-level supervisor
%%% @end

-module(zomp_sup).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").
-behavior(supervisor).

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by zomp:start/0.
%%
%% Spawns a single, registered worker process.
%%
%% Error conditions, supervision strategies and other important issues are
%% explained in the supervisor module docs:
%% http://erlang.org/doc/man/supervisor.html

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% Do not call this function directly -- it is exported only because it is a
%% necessary part of the OTP supervisor behavior.

init(none) ->
    RestartStrategy = {rest_for_one, 1, 60},

    Realms   = {zomp_realms,
                {zomp_realms, start_link, []},
                permanent,
                20000,
                supervisor,
                [zomp_realms]},

    Clients  = {zomp_clients,
                {zomp_clients, start_link, []},
                permanent,
                5000,
                supervisor,
                [zomp_clients]},

    Nodes    = {zomp_nodes,
                {zomp_nodes, start_link, []},
                permanent,
                5000,
                supervisor,
                [zomp_nodes]},

    Children = [Realms, Clients, Nodes],
    {ok, {RestartStrategy, Children}}.
