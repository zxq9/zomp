%%% @doc
%%% Zomp Upstream Node Connection
%%%
%%% This module abstracts upstream node connections.
%%%
%%% This module is organized into the following sections:
%%% - Constants, Defintions, and Types
%%% - Interface
%%% - Initialization
%%% - Service Loop
%%% - Handlers
%%% - Utility Functions
%%% @end

-module(zomp_node).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start/3, enroll/2, fetch/2, pull_event/3, pull_span/3, subscribe/2, stop/1]).
-export([start_link/3, init/4]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).

-include("$zx_include/zx_logger.hrl").


%%% Constants, Definitions, and Types

-define(TIMEOUT, 5000).

-record(s, {socket  = none :: none |  gen_tcp:socket(),
            fetches = #{}  :: #{zx:package_id() := fetch()}}).

-type state()        :: #s{}.
-type fetch()        :: {Status     :: fetch_status(),
                         Requestors :: [pid()]}.
-type fetch_status() :: wait | pos_integer().


%%% Interface

-spec subscribe(Node, Realm) -> ok
    when Node  :: pid(),
         Realm :: zomp:realm().
%% @doc
%% Subscribe to a given realm on the connected node.

subscribe(Node, Realm) ->
    Node ! {subscribe, self(), Realm},
    ok.


-spec enroll(Node, Realm) -> ok
    when Node  :: pid(),
         Realm :: zx:realm().

enroll(Node, Realm) ->
    Node ! {enroll, Realm},
    ok.

-spec fetch(Node, PackageID) -> ok
    when Node      :: pid(),
         PackageID :: zomp:package_id().
%% @doc
%% Make an upstream request for the given package.

fetch(Node, PackageID) ->
    Node ! {fetch, self(), PackageID},
    ok.


-spec pull_event(Node, Realm, Serial) -> ok
    when Node   :: pid(),
         Realm  :: zx:realm(),
         Serial :: zx:serial().

pull_event(Node, Realm, Serial) ->
    Node ! {pull_event, Realm, Serial},
    ok.


-spec pull_span(Node, Realm, Span) -> ok
    when Node  :: pid(),
         Realm :: zx:realm(),
         Span  :: {Start :: zx:serial(), End :: zx:serial()}.

pull_span(Node, Realm, Span) ->
    Node ! {pull_span, Realm, Span},
    ok.



%%% Startup

-spec start(Target, Desired, ExternalPort) -> Result
    when Target       :: zx:host(),
         Desired      :: [zx:realm()],
         ExternalPort :: inet:port_number(),
         Result       :: {ok, pid()}
                       | {error, term()}.
%% @doc
%% Spawns a new zomp_node process to establish a NODE connection to an upstream
%% node.

start(Target, Desired, ExternalPort) ->
    zomp_node_sup:start_node(Target, Desired, ExternalPort).


-spec stop(Node) -> ok
    when Node :: pid().
%% @doc
%% Tells the node connector to halt whatever it is doing and terminate immediately.

stop(Node) ->
    Node ! stop,
    ok.



%%% Initialization

-spec start_link(Target, Desired, ExternalPort) -> Result
    when Target       :: zx:host(),
         Desired      :: [zx:realm()],
         ExternalPort :: inet:port_number(),
         Result       :: {ok, pid()}
                       | {error, term()}.
%% @private
%% Called by zomp_node_sup.

start_link(Target, Desired, ExternalPort) ->
    proc_lib:start_link(?MODULE, init, [self(), Target, Desired, ExternalPort]).


-spec init(Parent, Target, Desired, ExternalPort) -> no_return()
    when Parent       :: pid(),
         Target       :: zomp:host(),
         Desired      :: [zx:realm()],
         ExternalPort :: inet:port_number().
%% @private
%% Called by zomp_node_sup as part of the proc_lib OTP compliant initialization.

init(Parent, Target, Desired, ExternalPort) ->
    HostString = zx_net:host_string(Target),
    ok = log(info, "Starting node connector for ~ts", [HostString]),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    connect_host(Parent, Debug, Target, Desired, ExternalPort).


-spec connect_host(Parent, Debug, Target, Desired, EP) -> no_return()
    when Parent  :: pid(),
         Debug   :: [sys:dbg_opt()],
         Target  :: zx:host(),
         Desired :: [zx:realm()],
         EP      :: inet:port_number().

connect_host(Parent, Debug, Target = {Address, Port}, Desired, EP) ->
    Options = [{packet, 4}, {mode, binary}, {nodelay, true}, {active, once}],
    case gen_tcp:connect(Address, Port, Options, 5000) of
        {ok, Socket} ->
            confirm_host(Parent, Debug, Socket, Desired, EP);
        {error, Reason} ->
            HostString = zx_net:host_string(Target),
            ok = log(info, "Connection to ~ts failed with: ~tp", [HostString, Reason]),
            terminate()
    end.


-spec confirm_host(Parent, Debug, Socket, Desired, EP) -> no_return()
    when Parent  :: pid(),
         Debug   :: [sys:dbg_opt()],
         Socket  :: gen_tcp:socket(),
         Desired :: [zx:realm()],
         EP      :: inet:port_number().
%% @private
%% Send the initial connection token and confirm receipt of the status of realms hosted
%% by the node.
%%
%% NOTE:
%%     The first receive clause below cheats by avoiding a call to binary_to_term and
%%     a try..catch construct by matching on the exact binary that results from
%%     `term_to_binary(ok)'. Instead of being a speed hack, it permits the clause that
%%      should be receiving `{redirect, Next}' to be explicit and isolated.

confirm_host(Parent, Debug, Socket, Desired, EP) ->
    TermBin = term_to_binary(Desired),
    ok = gen_tcp:send(Socket, <<"ZOMP NODE 1:", EP:16, TermBin/binary>>),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:8, Bin/binary>>} ->
            {ok, Available} = zx_lib:b_to_ts(Bin),
            ok = zomp_node_man:report({connected, Available}),
            loop(Parent, Debug, #s{socket = Socket});
        {tcp, Socket, <<1:8, Bin/binary>>} ->
            {ok, Targets} = zx_lib:b_to_ts(Bin),
            ok = zomp_node_man:report({redirect, Targets}),
            terminate(Socket);
        {tcp, Socket, <<2:8>>} ->
            ok = log(warning, "Upstream node rejected protocol v1"),
            terminate(Socket);
        {tcp, Socket, <<2:8, Version:16>>} ->
            Message = "Upstream node rejected protocol v1, required v~w",
            ok = log(warning, Message, [Version]),
            terminate(Socket);
        {tcp, Socket, <<3:8, Reason/utf8>>} ->
            ok = log(warning, "Upstream node refused service with: ~200tp", [Reason]),
            terminate(Socket);
        {tcp_closed, Socket} ->
            ok = log(info, "Socket close on connect. Retiring."),
            terminate();
        stop ->
            terminate(Socket)
        after 5000 ->
            {ok, Host} = inet:peername(Socket),
            HostString = zx_net:host_string(Host),
            ok = log(info, "Connect response to ~ts timed out", [HostString]),
            terminate(Socket)
    end.



%%% Service Loop

-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% Main service loop.

loop(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, Message} ->
            NewState = dispatch(Message, State),
            loop(Parent, Debug, NewState);
        {fetch, Requestor, PackageID} ->
            NewState = do_fetch(Requestor, PackageID, State),
            loop(Parent, Debug, NewState);
        {enroll, Realm} ->
            ok = do_enroll(Realm, Socket),
            loop(Parent, Debug, State);
        {pull_event, Realm, Number} ->
            ok = do_pull_event(Realm, Number, Socket),
            loop(Parent, Debug, State);
        {pull_span, Realm, Range} ->
            ok = do_pull_span(Realm, Range, Socket),
            loop(Parent, Debug, State);
        stop ->
            {ok, Host} = inet:peername(Socket),
            HostString = zx_net:host_string(Host),
            ok = log(info, "Disconnecting from ~200tp.", [HostString]),
            terminate(Socket);
        {system, From, Request} ->
            Continue = {loop, State},
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, Continue);
        Unexpected ->
            ok = log(warning, "Unexpected message: ~200tp", [Unexpected]),
            loop(Parent, Debug, State)
    end.



%%% Message dispatch

-spec dispatch(Message, State) -> NewState | no_return()
    when Message  :: binary(),
         State    :: state(),
         NewState :: state().
%% @private
%% Interprets and validates an incoming message from the upstream node before passing
%% it on to dispatch/2 to be acted upon.

dispatch(<<1:1, 0:7>>, State = #s{socket = Socket}) ->
    ok = pong(Socket),
    State;
dispatch(<<1:1, 1:7, SS/binary>>, State) ->
    <<Size:24, _:Size/binary, _:8, TermBin/binary>> = SS,
    {ok, Term} = zx_lib:b_to_ts(TermBin),
    Realm =
        case element(4, Term) of
            {R, _}                             -> R;
            {_, #{package_id := {R, _, _}}, _} -> R
        end,
    {ok, Pid} = zomp_realm_man:lookup(Realm),
    ok = zomp_realm:update(Pid, SS),
    State;
dispatch(<<1:1, 2:7, Hosts/binary>>, #s{socket = Socket}) ->
    {ok, Targets} = zx_lib:b_to_ts(Hosts),
    zomp_node_man:report({redirect, Targets}),
    terminate(Socket);
dispatch(<<1:1, 3:7, Distance:8, TermBin/binary>>, State) ->
    {ok, PackageID} = zx_lib:b_to_ts(TermBin),
    hop_update(Distance, PackageID, State);
dispatch(Unknown, #s{socket = Socket}) ->
    {ok, Host} = inet:peername(Socket),
    HostString = zx_net:host_string(Host),
    Message = "Malformed node message from ~ts: ~200tp",
    ok = log(warning, Message, [HostString, Unknown]),
    terminate(Socket).



%%% Handlers

-spec pong(gen_tcp:socket()) -> ok.

pong(Socket) ->
    gen_tcp:send(Socket, <<1:1, 0:7>>).


-spec do_enroll(zx:realm(), gen_tcp:socket()) -> ok | no_return().

do_enroll(Realm, Socket) ->
    Command = 1,
    Bin = term_to_binary(Realm),
    ok = gen_tcp:send(Socket, <<0:1, Command:7, Bin/binary>>),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:8, ReplyBin/binary>>} ->
            case zx_lib:b_to_ts(ReplyBin) of
                error ->
                    Message = "Bad message from upstream on do_enroll/2: ~200tp",
                    ok = log(warning, Message, [ReplyBin]),
                    terminate(Socket);
                {ok, Serial} ->
                    zomp_node_man:report({enrolled, {Realm, Serial}})
            end;
        {tcp, Socket, Error} ->
            Reason = zx_net:err_in(Error),
            Message = "do_enroll(~tp, ~p) failed: ~200tp",
            ok = log(warning, Message, [Realm, Socket, Reason]),
            terminate(Socket)
        after 5000 ->
            Message = "do_enroll(~tp, ~p) failed: ~200tp",
            ok = log(warning, Message, [Realm, Socket, timeout]),
            terminate(Socket)
    end.


-spec do_pull_event(zx:realm(), zx:serial(), gen_tcp:socket()) -> ok.

do_pull_event(Realm, Number, Socket) ->
    Command = 3,
    Payload = term_to_binary({Realm, Number}),
    Message = <<0:1, Command:7, Payload/binary>>,
    gen_tcp:send(Socket, Message).


-spec do_pull_span(Realm, Range, Socket) -> ok | no_return()
    when Realm  :: zx:realm(),
         Range  :: {Start :: zx:serial(), End :: zx:serial()},
         Socket :: gen_tcp:socket().

do_pull_span(Realm, Range, Socket) ->
    Command = 4,
    Payload = term_to_binary({Realm, Range}),
    Message = <<0:1, Command:7, Payload/binary>>,
    gen_tcp:send(Socket, Message).


-spec do_fetch(Requestor, PackageID, State) -> NewState
    when Requestor :: pid(),
         PackageID :: zx:package_id(),
         State     :: state(),
         NewState  :: state().

do_fetch(Requestor, PackageID, State = #s{socket = Socket, fetches = Fetches}) ->
    NewFetches =
        case maps:find(PackageID, Fetches) of
            {ok, {wait, Requestors}} ->
                log(info, "Joined ongoing requestors."),
                NewRequestors = add_unique(Requestor, Requestors),
                maps:update(PackageID, {wait, NewRequestors}, Fetches);
            {ok, {Distance, Requestors}} ->
                log(info, "Joined ongoing requestors."),
                Requestor ! {hops, PackageID, Distance},
                NewRequestors = add_unique(Requestor, Requestors),
                maps:update(PackageID, {Distance, NewRequestors}, Fetches);
            error ->
                Command = 6,
                Bin = term_to_binary(PackageID),
                Request = <<Command:8, Bin/binary>>,
                ok = gen_tcp:send(Socket, Request),
                log(info, "Sending fetch request upstream"),
                maps:put(PackageID, {wait, [Requestor]}, Fetches)
        end,
    State#s{fetches = NewFetches}.

add_unique(Requestor, Requestors) ->
    case lists:member(Requestor, Requestors) of
        false -> [Requestor | Requestors];
        true  -> Requestors
    end.


-spec hop_update(Distance, PackageID, State) -> NewState | no_return()
    when Distance  :: non_neg_integer(),
         PackageID :: zx:package_id(),
         State     :: state(),
         NewState  :: state().

hop_update(0,
           PackageID = {Realm, Name, Version},
           State = #s{socket = Socket, fetches = Fetches}) ->
    {{_, Requestors}, NewFetches} = maps:take(PackageID, Fetches),
    ok = notify(0, PackageID, Requestors),
    {ok, Bin} = zx_net:rx(Socket),
    {ok, RealmPid} = zomp_realm_man:lookup(Realm),
    ok = zomp_realm:cache(RealmPid, {Name, Version}, Bin),
    ok = forward(PackageID, Bin, Requestors),
    State#s{fetches = NewFetches};
hop_update(Distance, PackageID, State = #s{fetches = Fetches}) ->
    {_, Requestors} = maps:get(PackageID, Fetches),
    ok = notify(Distance, PackageID, Requestors),
    NewFetches = maps:update(PackageID, {Distance, Requestors}, Fetches),
    State#s{fetches = NewFetches}.

notify(Hops, PackageID, Requestors) ->
    Notify = fun(R) -> R ! {hops, PackageID, Hops} end,
    lists:foreach(Notify, Requestors).

forward(PackageID, Bin, Requestors) ->
    Send = fun(R) -> R ! {ok, PackageID, Bin} end,
    lists:foreach(Send, Requestors).


%%% Utility functions

-spec terminate(gen_tcp:socket()) -> no_return().

terminate(Socket) ->
    ok = zx_net:disconnect(Socket),
    terminate().


-spec terminate() -> no_return().

terminate() ->
    exit(normal).



%%% OTP Conformance Functions

-spec system_continue(Parent, Debug, Continue) -> no_return()
    when Parent   :: pid(),
         Debug    :: list(),
         Continue :: {Next  :: loop,
                      State :: term()}.

system_continue(Parent, Debug, {loop, State}) ->
    loop(Parent, Debug, State).


-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: list(),
         State  :: state().

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).


-spec system_get_state(term()) -> {ok, term()}.

system_get_state(Misc) ->
    {ok, Misc}.


-spec system_replace_state(fun(), term()) -> {ok, term(), term()}.

system_replace_state(StateFun, Misc) ->
    {ok, StateFun(Misc), Misc}.
