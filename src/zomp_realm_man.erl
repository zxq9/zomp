%%% @doc
%%% Zomp Realm Manager
%%%
%%% This process acts as the system-wide general interface to realms serviced by this
%%% node. Its primary responsibilities are to manage the configuration of realms on
%%% the node, ensure all realms are started, and act as a service directory for clients
%%% that need to know where the realms are located.
%%%
%%% When a realm's status is updated the rest of the system has stale data temporarily.
%%% It is the job of this process to notify adjacent managers of status updates.
%%% If a realm handler crashes or experiences a serial update this process will notify
%%% the rest of the system. Delays are usually too short to affect operation, but
%%% in the event of a slow realm restart a client may receive a "service unavilable"
%%% message or timeout and wind up seeking another node.
%%% @end

-module(zomp_realm_man).
-vsn("0.6.3").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([lookup/1]).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-include("$zx_include/zx_logger.hrl").



%%% Type Definitions

-record(s,
        {daemon = none :: none | reference(),
         realms = []   :: [realm_record()]}).

-record(realm,
        {name :: zx:realm(),
         pid  :: pid(),
         mon  :: reference()}).

-type state()        :: #s{}.
-type realm_record() :: #realm{}.


%%% Interface Functions

-spec lookup(zx:realm()) -> {ok, pid()} | {error, bad_realm}.

lookup(Realm) ->
    gen_server:call(?MODULE, {lookup, Realm}).


%%% Startup

-spec start_link() -> {ok, pid()} | {error, term()}.
%% @private
%% Startup function -- intended to be called by supervisor.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
%% @private
%% gen_server callback for startup

init(none) ->
    ok = log(info, "Starting"),
    {ok, Realms} = zx_daemon:conf(realms),
    Mon = monitor(process, zx_daemon),
    State = #s{daemon = Mon,
               realms = lists:map(start_worker(), Realms)},
    {ok, State}.


-spec start_worker() -> fun((Realm) -> Record)
    when Realm  :: zx:realm(),
         Record :: realm_record().

start_worker() ->
    {ok, Managed} = zx_daemon:conf(managed),
    fun (Realm) ->
        Role =
            case lists:member(Realm, Managed) of
                true  -> prime;
                false -> distributor
            end,
        {ok, Pid} = zomp_realm_sup:start_realm(Realm, Role),
        Mon = monitor(process, Pid),
        #realm{name = Realm, pid = Pid, mon = Mon}
    end.



%%% gen_server Message Handling Callbacks

handle_call({lookup, Realm}, _, State) ->
    Result = do_lookup(Realm, State),
    {reply, Result, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tw: ~tw~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tw~n", [Unexpected]),
    {noreply, State}.


handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Mon, Pid, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tw~n", [Unexpected]),
    {noreply, State}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
%% @private
%% The gen_server:code_change/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
%% @private
%% The gen_server:terminate/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2

terminate(_, _) ->
    ok.



%%% Doer Functions

-spec do_lookup(zx:realm(), state()) -> {ok, pid()} | {error, bad_realm}.

do_lookup(Realm, #s{realms = Realms}) ->
    case lists:keyfind(Realm, #realm.name, Realms) of
        #realm{pid = Pid} -> {ok, Pid};
        false             -> {error, bad_realm}
    end.
    

-spec handle_down(Mon, Pid, Reason, State) -> NewState
    when Mon      :: reference(),
         Pid      :: pid(),
         Reason   :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% Revive dead realms.

handle_down(Mon, Pid, shutdown, State = #s{daemon = Mon}) ->
    ok = log(info, "zx_daemon ~p shutdown. Waiting for the darkness...", [Pid]),
    State#s{daemon = none};
handle_down(Mon, Pid, Reason, State = #s{daemon = Mon}) ->
    ok = tell(warning, "zx_daemon ~p went down with ~p. Re-establishing zomp mode.",
              [Pid, Reason]),
    NewMon = monitor(process, zx_daemon),
    ok = zx_daemon:zomp_mode(),
    State#s{daemon = NewMon};
handle_down(Mon, Pid, Reason, State = #s{realms = Realms}) ->
    case lists:keytake(Mon, #realm.mon, Realms) of
        {value, #realm{name = Realm, pid = Pid}, NextRealms} ->
            Start = start_worker(),
            NewRealm = Start(Realm),
            NewRealms = [NewRealm | NextRealms],
            State#s{realms = NewRealms};
        false ->
            Unexpected = {'DOWN', Mon, process, Pid, Reason},
            ok = tell(warning, "~w Unexpected info: ~tw~n", [self(), Unexpected]),
            State
    end.
