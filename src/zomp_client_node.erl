%%% @doc
%%% Zomp Client User
%%% 
%%% This module implements "user" level utility connections.
%%%
%%%
%%% User mode clients can take one of four roles:
%%% - Anonymous User
%%% - Authenticated User
%%% - Packager
%%% - Manager
%%%
%%% All four client-side user roles are implemented in the zx client tool.
%%%
%%% - Constants, Definitions, and Types
%%% - Interface
%%% @end

-module(zomp_client_node).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start_session/4, forward_event/2, redirect/2, change_realm/3]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).

-include("$zx_include/zx_logger.hrl").

%% Constants, Definitions, and Types


-record(s, {socket   = none       :: none | gen_tcp:socket(),
            timer    = none       :: none | reference(),
            realms   = #{}        :: #{zx:realm() := pid()},
            carried  = []         :: [zx:realm()],
            enrolled = sets:new() :: enrollments(),
            port     = vamp       :: inet:port_number()}).

-type state() :: #s{}.

% TODO: This is still unused. A zomp_realm crash will silently drop enrollments.
-type enrollments()  :: sets:set(zx:realm()).



%%% Interface

-spec start_session(Parent, Debug, Socket, Bin) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         Socket :: gen_tcp:socket(),
         Bin    :: binary().

start_session(Parent, Debug, Socket, <<Port:16, Bin/binary>>) ->
    {ok, Host = {Address, ConnPort}} = zx_net:peername(Socket),
    ok = log(info, "Connection from ~s:~w", [inet:ntoa(Address), ConnPort]),
    case zx_lib:b_to_ts(Bin) of
        {ok, Realms} when Port == 0 ->
            State = #s{socket = Socket, carried = Realms, port = vamp},
            maybe_accept(Parent, Debug, Host, State);
        {ok, Realms} ->
            log(info, "Port: ~tp, Realms: ~tp", [Port, Realms]),
            State = #s{socket = Socket, carried = Realms, port = Port},
            assess_node(Parent, Debug, Address, State);
        error ->
            ok = gen_tcp:send(Socket, <<3:8, "Malformed message.">>),
            ok = zx_net:disconnect(Socket),
            terminate()
    end.


-spec forward_event(Node, Event) -> ok
    when Node  :: pid(),
         Event :: binary().

forward_event(Node, Event) ->
    Node ! {forward, Event},
    ok.


-spec redirect(Node, Hosts) -> ok
    when Node  :: pid(),
         Hosts :: [{zx:host(), [zx:realm()]}].

redirect(Node, Hosts) ->
    Node ! {redirect, Hosts},
    ok.


-spec change_realm(Leaf, Realm, Pid) -> ok
    when Leaf  :: pid(),
         Realm :: zx:realm(),
         Pid   :: pid().

change_realm(Leaf, Realm, Pid) ->
    Leaf ! {change, Realm, Pid},
    ok.



%%% Session Init

-spec assess_node(Parent, Debug, Address, State) -> no_return()
    when Parent  :: pid(),
         Debug   :: term(),
         Address :: inet:ip_address(),
         State   :: state().
%% @private
%% Check whether the connecting node is sending its public service port, and if so
%% proceed to check whether it is reachable and valid.

assess_node(Parent, Debug, Address, State = #s{port = Port}) ->
    Options = [{packet, 4}, {mode, binary}, {nodelay, true}, {active, once}],
    case gen_tcp:connect(Address, Port, Options, 3000) of
        {ok, TestSock}   -> assess_node2(Parent, Debug, TestSock, State);
        {error, timeout} -> maybe_accept(Parent, Debug, none, State#s{port = vamp})
    end.


assess_node2(Parent, Debug, TestSock, State) ->
    Host =
        case zx_net:peername(TestSock) of
            {ok, H}    -> H;
            {error, _} -> none
        end,
    ok = gen_tcp:send(TestSock, "ZOMP TEST 1"),
    receive
        {tcp, TestSock, <<"OK">>} ->
            ok = zx_net:disconnect(TestSock),
            ok = log(info, "Setting peer; node accepts connections."),
            maybe_accept(Parent, Debug, Host, State);
        {tcp, TestSock, Unexpected} ->
            ok = zx_net:disconnect(TestSock),
            ok = log(info, "Setting vamp; node talks crazy: ~200tp", [Unexpected]),
            maybe_accept(Parent, Debug, Host, State#s{port = vamp});
        {tcp_closed, TestSock} ->
            ok = log(info, "Setting vamp; node dropped connection."),
            maybe_accept(Parent, Debug, Host, State#s{port = vamp})
        after 3000 ->
            ok = zx_net:disconnect(TestSock),
            ok = log(info, "Setting vamp; node timed out."),
            maybe_accept(Parent, Debug, none, State#s{port = vamp})
    end.


-spec maybe_accept(Parent, Debug, Host, State) -> no_return()
    when Parent :: pid(),
         Debug  :: term(),
         Host   :: none | zx:host(),
         State  :: state().
%% @private
%% Ask zomp_client_man whether to accept the downstream node connection based on
%% the current state of the local node and the reported downstream realm status.

maybe_accept(Parent,
             Debug,
             Host,
             State = #s{carried = Desired, port = vamp}) ->
    Result = zomp_client_man:connected(vamp, Host, Desired),
    maybe_accept2(Parent, Debug, Result, State);
maybe_accept(Parent,
             Debug,
             Host,
             State = #s{carried = Desired}) ->
    Result = zomp_client_man:connected(node, Host, Desired),
    maybe_accept2(Parent, Debug, Result, State).
    
maybe_accept2(Parent, Debug, {accept, Available}, State = #s{socket = Socket}) ->
    {ReportBin, Local} = unpack(Available, [], #{}),
    log(info, "Sending acceptance to client."),
    ok = gen_tcp:send(Socket, <<0:8, ReportBin/binary>>),
    Timer = erlang:send_after(60000, self(), ping),
    loop(Parent, Debug, State#s{timer = Timer, realms = Local});
maybe_accept2(_, _, {redirect, Nodes}, #s{socket = Socket}) ->
    NodesBin = term_to_binary(Nodes),
    ok = gen_tcp:send(Socket, <<1:8, NodesBin/binary>>),
    ok = zx_net:disconnect(Socket),
    terminate().

unpack([{Realm, Serial, Pid} | Rest], Report, Local) ->
    unpack(Rest, [{Realm, Serial} | Report], maps:put(Realm, Pid, Local));
unpack([], Report, Local) ->
    {term_to_binary(Report), Local}.



%%% Service Loop

-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: term(),
         State  :: state().
%% @private
%% Main service loop.

loop(Parent, Debug, State = #s{realms = Realms, socket = Socket, timer = Timer}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:1, Code:7, Reference/binary>>} ->
            _ = erlang:cancel_timer(Timer),
            {ok, ID} = zx_lib:b_to_ts(Reference),
            ok =
                case interpret(Code, ID, State) of
                    {reply, Result} -> gen_tcp:send(Socket, Result);
                    noreply         -> ok
                end,
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        ping ->
            ok = ping(Socket),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        {forward, Event} ->
            _ = erlang:cancel_timer(Timer),
            Message = <<1:1, 1:7, Event/binary>>,
            ok = gen_tcp:send(Socket, Message),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        {redirect, Hosts} ->
            Message = <<1:1, 2:7, (term_to_binary(Hosts))/binary>>,
            ok = gen_tcp:send(Socket, Message),
            ok = zx_net:disconnect(Socket),
            terminate();
        {change, Realm, Pid} ->
            NewRealms = maps:put(Realm, Pid, Realms),
            loop(Parent, Debug, State#s{realms = NewRealms});
        {tcp_closed, Socket} ->
            ok = log(info, "Socket closed, retiring."),
            terminate();
        disconnect ->
            _ = erlang:cancel_timer(Timer),
            ok = zomp_net:disconnect(Socket),
            ok = log(info, "'disconnect' received. Closing socket and retiring."),
            terminate();
        {system, From, Request} ->
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State);
        Unexpected ->
            ok = log(warning, "Unexpected message: ~250tp", [Unexpected]),
            loop(Parent, Debug, State)
    end.


-spec ping(gen_tcp:socket()) -> ok | no_return().

ping(Socket) ->
    ok = gen_tcp:send(Socket, <<1:1, 0:7>>),
    receive
        {tcp, Socket, <<1:1, 0:7>>} ->
            ok;
        {tcp_closed, Socket} ->
            exit(normal)
        after 5000 ->
            ok = zx_net:disconnect(Socket),
            exit(normal)
    end.


-spec interpret(Code, ID, State) -> Result
    when Code   :: 1..7,
         ID     :: zx:realm()
                 | zx:package()
                 | {zx:realm(), zx:serial()}
                 | {zx:realm(), {zx:serial(), zx:serial()}}
                 | zx:package_id()
                 | zx:key_id()
                 | {zx:realm(), zx:serial()}
                 | {zx:realm(), zx:serial(), zx:serial()},
         State  :: state(),
         Result :: {reply, iodata()}
                 | noreply.

interpret(Code, {R, X, Y}, State) -> interpret(Code, R, {X, Y}, State);
interpret(Code, {R, X}, State)    -> interpret(Code, R, X, State);
interpret(Code, R, State)         -> interpret(Code, R, none, State).


interpret(Code, Realm, Target, #s{socket = Socket, realms = Realms}) ->
    log(info, "Received Code: ~tp, Realm: ~tp, Target: ~200tp", [Code, Realm, Target]),
    Result =
        case maps:find(Realm, Realms) of
            {ok, Pid} -> dispatch(Code, Pid, Realm, Target, Socket);
            error     -> {error, bad_realm}
        end,
    log(info, "Result: ~250tp", [Result]),
    exterpret(Result).


dispatch(Code, Pid, Realm, Target, Socket) ->
    case Code of
        1 -> zomp_realm:enroll(Pid);
        2 -> zomp_realm:disenroll(Pid);
        3 -> pull_event(Pid, Target);
        4 -> pull_span(Pid, Target);
        5 -> zomp_realm:pull_history(Pid);
        6 -> zomp_client:fetch(Pid, Realm, Target, Socket);
        7 -> zomp_client:sync_keys(Pid, Target, Socket);
        _ -> {error, bad_message}
    end.


exterpret(ok)              -> {reply, <<0:1, 0:7>>};
exterpret({ok, Response})  -> {reply, <<0:1, 0:7, (term_to_binary(Response))/binary>>};
exterpret({error, Reason}) -> {reply, zx_net:err_ex(Reason)};
exterpret(noreply)         -> noreply;
exterpret(done)            -> noreply.


pull_event(Pid, Target) ->
    ok = zomp_realm:pull_event(Pid, Target),
    noreply.


pull_span(Pid, Target) ->
    ok = zomp_realm:pull_span(Pid, Target),
    noreply.


-spec terminate() -> no_return().
%% @private
%% Convenience wrapper around the suicide call.
%% In the case that a more formal retirement procedure is required, consider notifying
%% the supervisor with `supervisor:terminate_child(zomp_client_sup, PID)' and writing
%% a proper system_terminate/2.

terminate() ->
    exit(normal).



%%% OTP Conformance Functions

-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: list(),
         State  :: state().

system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).


-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: list(),
         State  :: state().

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).


-spec system_get_state(term()) -> {ok, term()}.

system_get_state(Misc) -> {ok, Misc}.


-spec system_replace_state(fun(), term()) -> {ok, term(), term()}.

system_replace_state(StateFun, Misc) ->
    {ok, StateFun(Misc), Misc}.
