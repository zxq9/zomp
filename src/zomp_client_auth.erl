%%% @doc
%%% Zomp Client Auth
%%%
%%% This module implements authenticated client functionality for:
%%% - Packagers
%%% - Maintainers
%%% - Sysops.
%%%
%%% The functionality provided by this module does not fit a persistent connection
%%% model, unlike the zomp_client_leaf and zomp_client_node modules. For this reason
%%% this module is written as a chain of socket-oriented, single operation-and-close
%%% procedures, similar in nature to HTTP/1.0.
%%%
%%% This module is entered via interpret/2.
%%% Some operations may involve considerable back-and-forth communication and arbitrary
%%% looping, but there is no central service loop in this module.
%%%
%%% The flow of execution is:
%%%  1. Receive the initial request, and determine whether it is a NULL request or not.
%%%   1.a. If a NULL request, send an SS tag and await the actual command message.
%%%  2. Dispatch over the command message.
%%%  3. The result is exterpreted and a response is sent if necessary.
%%%  4. Connection is closed and the process exits.
%%% @end

-module(zomp_client_auth).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([interpret/2]).

-include("$zx_include/zx_logger.hrl").


%%% Interface

-spec interpret(Bin, Socket) -> no_return()
    when Bin    :: binary(),
         Socket :: gen_tcp:socket().

interpret(<<Size:24, Sig:Size/binary, Signed/binary>>, Socket) ->
    <<Code:8, TermBin/binary>> = Signed,
    Result =
        case zx_lib:b_to_ts(TermBin) of
            {ok, {R,X,Y,Z}} -> maybe_dispatch(R, Code, Sig, Signed, {X,Y,Z}, Socket);
            {ok, {R,X,Y}}   -> maybe_dispatch(R, Code, Sig, Signed, {X,Y}, Socket);
            {ok, {R,X}}     -> maybe_dispatch(R, Code, Sig, Signed, X, Socket);
            {ok, R}         -> maybe_dispatch(R, Code, Sig, Signed, none, Socket);
            error           -> {error, bad_message}
        end,
    exterpret(Result, Socket).


maybe_dispatch(Realm, Code, Sig, Signed, Data, Socket) ->
    ok = tell("Received code ~p for realm ~p", [Code, Realm]),
    {ok, Managed} = zx_daemon:conf(managed),
    case lists:member(Realm, Managed) of
        true ->
            {ok, Pid} = zomp_realm_man:lookup(Realm),
            maybe_null(Code, Sig, Signed, Data, Pid, Socket);
        false ->
            {error, not_prime}
    end.

maybe_null(0, Sig, Signed, Data, Realm, Socket) ->
    null(Sig, Signed, Data, Realm, Socket);
maybe_null(Code, Sig, Signed, Data, Realm, Socket) ->
    dispatch(Code, Sig, Signed, Data, Realm, Socket).


null(Sig, Signed, {SentTS, Signatory, KeyName}, Realm, Socket) ->
    ReceivedTS = os:timestamp(),
    Diff = timer:now_diff(ReceivedTS, SentTS),
    ok = log(info, "Requestor clock offset: ~pµs", [Diff]),
    case zomp_realm:null(Realm, ReceivedTS, Signatory, KeyName) of
        {ok, Tag, PubKey} ->
            null2(Sig, Signed, Signatory, KeyName, PubKey, Tag, Realm, Socket);
        Error ->
            Error
    end.

null2(Sig, Signed, Signatory, KeyName, PubKey, Tag, Realm, Socket) ->
    case zx_key:verify(Signed, Sig, PubKey) of
        true ->
            Bin = term_to_binary(Tag),
            ok = gen_tcp:send(Socket, <<0:1, 0:7, Bin/binary>>),
            null3(Signatory, KeyName, PubKey, Tag, Realm, Socket);
        false ->
            ok = zomp_realm:clear_ss(Realm),
            {error, bad_sig}
    end.

null3(Signatory, KeyName, PubKey, Tag = {_, ReceivedTS}, Realm, Socket) ->
    ok = inet:setopts(Socket, [{active, once}]),
    {ok, Seconds} = zx_daemon:conf(timeout),
    Timeout = Seconds * 1000,
    receive
        {tcp, Socket, <<0:8, Message/binary>>} ->
            <<Size:24, Sig:Size/binary, Signed/binary>> = Message,
            <<Code:8, TermBin/binary>> = Signed,
            case zx_lib:b_to_ts(TermBin) of
                {ok, {Tag, Signatory, KeyName, Payload}} ->
                    Vett = {Tag, Signatory, KeyName, Payload},
                    Data = {PubKey, Message, Vett},
                    null4(ReceivedTS, Code, Sig, Signed, Data, Realm, Socket);
                {ok, Unexpected} ->
                    ok = tell(warning, "Unexpected request: ~250tp", [Unexpected]),
                    {error, bad_request};
                error ->
                    {error, bad_message}
            end;
        {tcp, Socket, <<Bad:8, _/binary>>} ->
            ok = log(warning, "Received problem code: ~w. Terminating.", [Bad]),
            ok = zx_net:disconnect(Socket),
            {error, bad_message};
        {tcp_closed, Socket} ->
            ok = log(info, "AUTH client dropped connection."),
            {error, tcp_closed}
        after Timeout ->
            ok = tell(info, "Timed out waiting for auth command at null3/6"),
            {error, timeout}
    end.

null4(ReceivedTS, Code, Sig, Signed, Data, Realm, Socket) ->
    Now = os:timestamp(),
    Elapsed = timer:now_diff(Now, ReceivedTS),
    {ok, Seconds} = zx_daemon:conf(timeout),
    Timeout = Seconds * 1000000,
    case Elapsed < Timeout of
        true  -> dispatch(Code, Sig, Signed, Data, Realm, Socket);
        false -> {error, timeout}
    end.


dispatch(Code, Sig, Signed, Data, Realm, Socket) ->
    case Code of
         1 -> zomp_realm:list_users(Realm);
         2 -> zomp_realm:list_packagers(Realm, Data);
         3 -> zomp_realm:list_maintainers(Realm, Data);
         4 -> zomp_realm:add_packager(Realm, {Sig, Signed, Data});
         5 -> zomp_realm:rem_packager(Realm, {Sig, Signed, Data});
         6 -> zomp_realm:add_maintainer(Realm, {Sig, Signed, Data});
         7 -> zomp_realm:rem_maintainer(Realm, {Sig, Signed, Data});
         8 -> zomp_realm:list_pending(Realm, Data);
         9 -> zomp_realm:list_approved(Realm);
        10 -> submit(Realm, Sig, Signed, Data, Socket);
        11 -> review(Realm, Data, Socket);
        12 -> zomp_realm:approve(Realm, {Sig, Signed, Data});
        13 -> zomp_realm:reject(Realm, {Sig, Signed, Data});
        14 -> accept(Realm, Sig, Signed, Data, Socket);
        15 -> ss(Realm, Sig, Signed, Data, fun zomp_realm:add_package/2);
        16 -> zomp_realm:add_user(Realm, {Sig, Signed, Data});
        17 -> zomp_realm:list_user_keys(Realm, Data);
        18 -> zomp_realm:add_user_keys(Realm, {Sig, Signed, Data});
        19 -> zomp_realm:keychain(Realm, Data);
%       20 -> ss(Realm, Sig, Signed, Data, fun zomp_realm:add_sysop/2);
%       21 -> ss(Realm, Sig, Signed, Data, fun zomp_realm:add_sysop_keys/2);
%       22 -> ss(Realm, Sig, Signed, Data, fun zomp_realm:rem_sysop/2);
         _ -> {error, bad_message}
    end.


exterpret(Result, Socket) ->
    Message =
        case Result of
            ok                  -> <<0:1, 0:7>>;
            {ok, tcp_closed}    -> exit(normal);
            {ok, Response}      -> <<0:1, 0:7, (term_to_binary(Response))/binary>>;
            {error, tcp_closed} -> exit(normal);
            {error, Reason}     -> zx_net:err_ex(Reason)
        end,
    ok = gen_tcp:send(Socket, Message),
    ok = zx_net:disconnect(Socket),
    exit(normal).



%%% Specialized Doer Functions

submit(Realm, Sig, Signed, {Signatory, KN, PV = {Package, _}}, Socket) ->
    ok = tell("Submitting package ~tp from ~tp", [Package, Signatory]),
    case zomp_realm:prep_submit(Realm, Signatory, KN, Package) of
        {ok, PK} -> submit2(Realm, Sig, Signed, KN, PK, PV, Socket);
        Error    -> Error
    end.

submit2(Realm, Sig, Signed, KeyName, PubKey, PV, Socket) ->
    case zx_key:verify(Signed, Sig, PubKey) of
        true  -> submit3(Realm, KeyName, PubKey, PV, Socket);
        false -> {error, bad_sig}
    end.

submit3(Realm, KeyName, PubKey, PV, Socket) ->
    case zx_net:rx(Socket) of
        {ok, Bin} -> submit4(Realm, KeyName, PubKey, PV, Bin);
        Error     -> Error
    end.

submit4(Realm, KeyName, PubKey, PV, Bin) ->
    <<Size:24, Sig:Size/binary, Signed/binary>> = Bin,
    case zx_key:verify(Signed, Sig, PubKey) of
        true ->
            zomp_realm:submit(Realm, PV, Bin);
        false ->
            Format = "Bad package signature. Use a different key than ~tp?",
            Message = io_lib:format(Format, [KeyName]),
            {error, Message}
    end.


review(Realm, Package, Socket) ->
    case zomp_realm:review(Realm, Package) of
        {ok, Bin} -> review2(Socket, Bin);
        Error     -> Error
    end.

review2(Socket, Bin) ->
    ok = gen_tcp:send(Socket, <<0:8>>),
    case zx_net:tx(Socket, Bin) of
        ok ->
            ok = zx_net:disconnect(Socket),
            {ok, tcp_closed};
        Error ->
            Error
    end.


accept(Realm, Sig, Signed, {PubKey, MessageBin, Vett}, Socket) ->
    case zx_key:verify(Signed, Sig, PubKey) of
        true  -> accept2(Realm, PubKey, MessageBin, Vett, Socket);
        false -> {error, bad_sig}
    end.

accept2(Realm,
        PubKey,
        MessageBin,
        {Tag, Signatory, KeyName, {Package, Meta, Size}},
        Socket) ->
    AcceptMeta = {Tag, Signatory, KeyName, {Meta, Size}},
    case zomp_realm:prep_accept(Realm, Package, MessageBin, AcceptMeta) of
        ok    -> accept3(Realm, PubKey, Tag, Socket);
        Error -> Error
    end.

accept3(Realm, PubKey, Tag, Socket) ->
    ok = gen_tcp:send(Socket, <<0:8>>),
    case zx_net:rx(Socket) of
        {ok, ZspBin} -> accept4(ZspBin, Realm, PubKey, Tag);
        Error        -> Error
    end.

accept4(ZspBin, Realm, PubKey, Tag) ->
    case zx_zsp:verify(ZspBin, PubKey) of
        true  -> zomp_realm:accept(Realm, Tag, ZspBin);
        false -> {error, bad_sig}
    end.


ss(Realm, Sig, Signed, {PubKey, Message, Data}, Action) ->
    case zx_key:verify(Signed, Sig, PubKey) of
        true  -> Action(Realm, {Message, Data});
        false -> {error, bad_sig}
    end.
