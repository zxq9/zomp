%%% @doc
%%% Zomp Client handler listener
%%%
%%% This module is where client handlers are spawned, where listening for an initial
%%% connection happens, and the place from which dispatch to a more specific module
%%% happens once the flavor of the protocol has been determined.
%%%
%%% Two three can be assumed by a connecting client:
%%% - `leaf': implemented by zomp_client_leaf
%%% - `node': implemented by zomp_client_node
%%% - `auth': implemented by zomp_client_auth
%%%
%%% Leaf clients are end-user systems -- systems querying the realm's registry, fetching
%%% source packages, building & running programs.
%%%
%%% Node clients are downstream Zomp nodes, and may be either "node" or "vampire" type,
%%% where a node is a publicly visible node and a "vampire" is a freeloader that isn't
%%% reachable by (or doesn't accept connections from) public nodes or clients.
%%%
%%% Auth clients are clients connecting for a single command session to a prime node,
%%% meaning the current node must be prime for the desired realm or things won't work
%%% out.
%%%
%%% This module listens on a TCP socket opened and owned by the named process
%%% zomp_client_man. Listeners are not pooled and queued, but instead each successive
%%% zomp_client worker is spawned by request of the previous one as soon as it accepts
%%% a TCP connection.
%%%
%%% The zomp_client processes are implemented here as "pure" Erlang processes, not as
%%% part of the gen_server behavior, making use of proc_lib instead to handle both the
%%% TCP socket and inter-process messaging directly in a single service loop. Each
%%% service loop is defined in the separate modules indicated above, this is only the
%%% connection's starting point.
%%%
%%% This module is organized into the following sections:
%%% - Initialization
%%% - OTP Conformance Functions
%%% @end

-module(zomp_client).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start/1]).
-export([forward/2, update/3, notify/2]).
-export([fetch/4, sync_keys/3]).
-export([start_link/1, init/2]).

-include("$zx_include/zx_logger.hrl").


%%% Initialization

-spec start(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: term().
%% @doc
%% Starts a listener on the provided ListenSocket.

start(ListenSocket) ->
    zomp_client_sup:start_acceptor(ListenSocket).



%%% External Notices

-spec forward(Client, SS) -> ok
    when Client :: pid(),
         SS     :: binary().

forward(Client, SS) ->
    Client ! {forward, SS},
    ok.


-spec notify(Client, PackageID) -> ok
    when Client    :: pid(),
         PackageID :: zx:package_id().

notify(Client, PackageID) ->
    Client ! {notify, PackageID},
    ok.


-spec update(Client, Realm, Serial) -> ok
    when Client :: pid(),
         Realm  :: zx:realm(),
         Serial :: zx:serial().

update(Client, Realm, Serial) ->
    Client ! {update, Realm, Serial},
    ok.



%%% Common Handlers

-spec fetch(Pid, Realm, Package, Socket) -> Result
    when Pid      :: pid(),
         Realm    :: zx:realm(),
         Package  :: {zx:name(), zx:version()},
         Socket   :: gen_tcp:socket(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_package
                   | bad_version
                   | timeout.

fetch(Pid, Realm, Package, Socket) ->
    fetch(Pid, Realm, Package, Socket, 0).

fetch(Pid, Realm, Package = {Name, Version}, Socket, Tries) ->
    case zomp_realm:fetch(Pid, Package) of
        {ok, Bin} -> advance(Socket, term_to_binary({Realm, Name, Version}), Bin);
        upstream  -> fetch_upstream(Pid, Realm, Package, Socket, Tries + 1);
        Error     -> Error
    end.


fetch_upstream(Pid, Realm, Package = {Name, Version}, Socket, Tries) ->
    ok = tell("Trying to fetch ~p upstream", [{Realm, Name, Version}]),
    case zomp_node_man:lookup(Realm) of
        {ok, Node} ->
            ok = tell("Found node connector at ~p", [Node]),
            PackageID = {Realm, Name, Version},
            ok = zomp_node:fetch(Node, PackageID),
            PIDB = term_to_binary(PackageID),
            wait_hops(PackageID, PIDB, Socket);
        wait ->
            wait_upstream_node(Pid, Realm, Package, Socket, Tries);
        error ->
            {error, bad_realm}
    end.


wait_hops(PackageID, PIDB, Socket) ->
    receive
        {ok, PackageID, Bin} ->
            advance(Socket, PIDB, Bin);
        {hops, PackageID, Distance} ->
            ok = tell("Fetch in progress. Hops: ~w", [Distance]),
            ok = gen_tcp:send(Socket, <<1:1, 3:7, Distance:8, PIDB/binary>>),
            wait_hops(PackageID, PIDB, Socket);
        {tcp_closed, Socket} ->
            terminate()
        after 60000 ->
            {error, timeout}
    end.


advance(Socket, PIDB, Bin) ->
    ok = gen_tcp:send(Socket, <<1:1, 3:7, 0:8, PIDB/binary>>),
    ok = zx_net:tx(Socket, Bin),
    done.


wait_upstream_node(Pid, Realm, Package, Socket, Tries) when Tries < 10 ->
    _ = erlang:send_after(1000, self(), retry),
    ok = tell("Waiting for upstream node connections to resolve..."),
    receive retry -> fetch(Pid, Realm, Package, Socket, Tries) end;
wait_upstream_node(_, _, _, _, _) ->
    {error, timeout}.


-spec sync_keys(Realm, KeyName, Socket) -> Result
    when Realm     :: pid(),
         KeyName   :: zx:key_name(),
         Socket    :: gen_tcp:socket(),
         Result    :: ok
                    | {error, Reason},
         Reason    :: bad_package
                    | bad_version
                    | timeout.
%% TODO

sync_keys(_, _, _) ->
    {error, "Not yet implemented"}.



%%% Callbacks

-spec start_link(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: term().
%% @private
%% Starts a zomp_client and links via proc_lib.
%% Do not call this directly, it is intended to be used by the zomp_client_sup.

start_link(ListenSocket) ->
    proc_lib:start_link(?MODULE, init, [self(), ListenSocket]).


-spec init(Parent, ListenSocket) -> no_return()
    when Parent       :: pid(),
         ListenSocket :: gen_tcp:socket().
%% @private
%% Standard OTP init/2 definition.
%% Do not call this directly, it is intended by be called via proc_lib.

init(Parent, ListenSocket) ->
    ok = tell("Listening"),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, ListenSocket).


-spec listen(Parent, Debug, ListenSocket) -> no_return()
    when Parent       :: pid(),
         Debug        :: [sys:dbg_opt()],
         ListenSocket :: gen_tcp:socket().
%% @private
%% This is where the zomp_client blocks to accept a TCP connection.
%% Once it acquires a connection it immediately spawns its succesor and transitions
%% to a dispatching state.

listen(Parent, Debug, ListenSocket) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} ->
            {ok, _} = start(ListenSocket),
            dispatch_client(Parent, Debug, Socket);
        {error, closed} ->
            ok = tell("Retiring: Listen socket closed."),
            terminate()
     end.


-spec dispatch_client(Parent, Debug, Socket) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         Socket :: gen_tcp:socket().
%% @private
%% Identify which service state to enter depending on what the connected client sends
%% after establishing a connection. In the case of a peek from another node, however,
%% return the peek value immediately and terminates, bypassing the service loop.
%% Service states are defined in zomp_client_user for downstream users and
%% zomp_client_node for downstream nodes.

dispatch_client(Parent, Debug, Socket) ->
    receive
        {tcp, Socket, <<"ZOMP LEAF 1:", Bin/binary>>} ->
            ok = log_client(Socket, "LEAF 1"),
            zomp_client_leaf:start_session(Parent, Debug, Socket, Bin);
        {tcp, Socket, <<"ZOMP NODE 1:", Bin/binary>>} ->
            ok = log_client(Socket, "NODE 1"),
            zomp_client_node:start_session(Parent, Debug, Socket, Bin);
        {tcp, Socket, <<"ZOMP AUTH 1:", Bin/binary>>} ->
            ok = log_client(Socket, "AUTH 1"),
            zomp_client_auth:interpret(Bin, Socket);
        {tcp, Socket, <<"ZOMP LEAF ", _/binary>>} ->
            ok = log_client(Socket, "LEAF [bad_version]"),
            ok = gen_tcp:send(Socket, <<2:8, 1:16>>),
            ok = zx_net:disconnect(Socket),
            terminate();
        {tcp, Socket, <<"ZOMP NODE ", _/binary>>} ->
            ok = log_client(Socket, "NODE [bad_version]"),
            ok = gen_tcp:send(Socket, <<2:8, 1:16>>),
            ok = zx_net:disconnect(Socket),
            terminate();
        {tcp, Socket, <<"ZOMP AUTH ", _/binary>>} ->
            ok = log_client(Socket, "AUTH [bad_version]"),
            ok = gen_tcp:send(Socket, <<2:8, 1:16>>),
            ok = zx_net:disconnect(Socket),
            terminate();
        {tcp, Socket, <<"ZOMP TEST 1">>} ->
            ok = gen_tcp:send(Socket, <<"OK">>),
            terminate();
        {tcp, Socket, Unexpected} ->
            {ok, {Addr, Port}} = zx_net:peername(Socket),
            Host = inet:ntoa(Addr),
            Message = "~ts:~w unexpected message: ~tw",
            ok = tell(warning, Message, [Host, Port, Unexpected]),
            ok = zx_net:disconnect(Socket),
            terminate();
        {tcp_closed, Socket} ->
            {ok, {Addr, Port}} = zx_net:peername(Socket),
            Host = inet:ntoa(Addr),
            ok = tell(warning, "~ts:~w closed unexpectedly on connect", [Host, Port]),
            terminate();
        {tcp_error, Socket, Reason} ->
            {ok, {Addr, Port}} = zx_net:peername(Socket),
            Host = inet:ntoa(Addr),
            ok = tell(error, "~ts:~w TCP error: ~w", [Host, Port, Reason]),
            ok = zomp_client_man:count_error(),
            terminate()
        after 5000 ->
            zx_net:disconnect(Socket),
            terminate()
    end.


log_client(Socket, Type) ->
    {ok, {Addr, Port}} = zx_net:peername(Socket),
    Host = inet:ntoa(Addr),
    tell("~ts:~w connected as ~s", [Host, Port, Type]).


-spec terminate() -> no_return().
%% @private
%% Convenience wrapper around the suicide call.
%% In the case that a more formal retirement procedure is required, consider notifying
%% the supervisor with `supervisor:terminate_child(zomp_client_sup, PID)' and writing
%% a proper system_terminate/2.

terminate() ->
    exit(normal).
