%%% @doc
%%% Zomp Node Service Supervisor
%%%
%%% The top-level supervisor for the Zomp node (upstream) service component.
%%% It owns the node manager (zomp_node_man) and the client supervisor (zomp_node_sup).
%%% Upstream nodes are represented by worker processes of the type zomp_node.
%%% @end

-module(zomp_nodes).
-vsn("0.6.3").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by zomp_sup
%%
%% Spawns a single, registered worker process.
%%
%% Error conditions, supervision strategies and other important issues are
%% explained in the supervisor module docs:
%% http://erlang.org/doc/man/supervisor.html

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% Do not call this function directly -- it is exported only because it is a
%% necessary part of the OTP supervisor behavior.

init(none) ->
    RestartStrategy = {one_for_one, 1, 60},

    NodeSup = {zomp_node_sup,
               {zomp_node_sup, start_link, []},
               permanent,
               brutal_kill,
               worker,
               [zomp_node_sup]},

    NodeMan = {zomp_node_man,
               {zomp_node_man, start_link, []},
               permanent,
               brutal_kill,
               worker,
               [zomp_node_man]},

    Children = [NodeSup, NodeMan],
    {ok, {RestartStrategy, Children}}.
