%%% @doc
%%% Client service manager
%%%
%%% This named process manages the high-level task of providing a client interface on
%%% the network. Information about current client subscriptions, peek counts, connection
%%% statistics, and listen/ignore functions are all implemented here.
%%%
%%% Listening for client connections is initiated by calling `listen/1' with the
%%% port number desired.
%%%
%%% Connecting clients are of either `leaf' or `node' types. A node connecting here
%%% can only be a downstream client node, as this module defines only node service
%%% functionality (the zomp_node_man module defines interactions with upstream).
%%%
%%% This module does not define any configuration file reading routines -- it should
%%% recieve all the data it requires to perform operations in the form of function
%%% parameters.
%%%
%%% This module is organized into the following sections:
%%% - Type Definitions
%%% - Service Interface
%%% - Startup
%%% - gen_server
%%% - Doer Functions
%%% @end

-module(zomp_client_man).
-vsn("0.6.3").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([listen/0, ignore/0]). % Control Interface
-export([update/2]).           % zomp_realm_man Interface
-export([connected/3]).        % Client Handler Interface
-export([start_link/0]).       % System & Callbacks
-export([init/1,
         handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-include("$zx_include/zx_logger.hrl").


-record(clients,
        {pids       = []    :: [pid()],
         max        = 16#10 :: pos_integer(),
         count      = 0     :: non_neg_integer()}).

-record(s,
        {listener = none                   :: none | gen_tcp:socket(),
         realms   = []                     :: [realm_record()],
         mons     = #{}                    :: #{pid() := mon_meta()},
         dist     = #{}                    :: #{pid() := zx:host()},
         node     = #clients{}             :: clients(),
         vamp     = #clients{}             :: clients(),
         leaf     = #clients{max = 16#100} :: clients(),
         total    = 0                      :: non_neg_integer()}).

-record(realm,
        {name         = none :: none | zx:realm(),
         pid          = none :: none | pid(),
         serial       = 0    :: zx:serial()}).

-type state()        :: #s{}.
-type realm_record() :: #realm{}.
-type clients()      :: #clients{}.
-type client_type()  :: node | vamp | leaf.
-type mon_meta()     :: {reference(), client_type()}.


%%% Service Interface

-spec listen() -> Result
    when Result  :: ok
                  | {error, Reason},
         Reason  :: {listening, inet:port_number()}
                  | [{ipv4 | ipv6, inet:posix()}].
%% @doc
%% Begin listening on IPv4 and IPv6 interfaces (if possible).

listen() ->
    gen_server:call(?MODULE, listen).


-spec ignore() -> ok.
%% @doc
%% Call to shut off all client listeners and close the listening port.

ignore() ->
    gen_server:cast(?MODULE, ignore).


-spec update(Realm, Serial) -> ok
    when Realm  :: zx:realm(),
         Serial :: zx:serial().

update(Realm, Serial) ->
    gen_server:cast(?MODULE, {update, Realm, Serial, self()}).


-spec connected(Type, Host, Desired) -> Response
    when Type     :: client_type(),
         Host     :: zx:host(),
         Desired  :: [zx:realm()],
         Response :: {accept, [{zx:realm(), zx:serial(), pid()}]}
                   | {redirect, [{zomp:realm(), zomp:node()}]}.
%% @doc
%% Requests connection acceptance or redirect targets.

connected(Type, Host, Desired) ->
    gen_server:call(?MODULE, {connected, Type, self(), Host, Desired}).



%%% Startup

-spec start_link() -> {ok, pid()} | {error, term()}.
%% @private
%% Startup function -- intended to be called by supervisor.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
%% @private
%% gen_server callback for startup.

init(none) ->
    {ok, Realms} = zx_daemon:conf(realms),
    Records = lists:map(fun populate/1, Realms),
    Initial = #s{realms = Records},
    State =
        case zx_daemon:conf(status) of
            {ok, listen} ->
                {ok, NextState} = do_listen(Initial),
                NextState;
            {ok, ignore} ->
                Initial
        end,
    {ok, State}.


populate(Realm) ->
    {ok, Pid} = zomp_realm_man:lookup(Realm),
    {ok, Serial} = zomp_realm:serial(Pid),
    #realm{name = Realm, pid = Pid, serial = Serial}.



%%% gen_server

%% @private
%% gen_server callback for OTP calls

handle_call(listen, _, State) ->
    {Response, NewState} = do_listen(State),
    {reply, Response, NewState};
handle_call({connected, Type, Pid, Host, Realms}, _, State) ->
    {Response, NewState} = do_connected(Type, {Pid, Host, Realms}, State),
    {reply, Response, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call ~tw: ~tw", [From, Unexpected]),
    {noreply, State}.


%% @private
%% gen_server callback for OTP casts

handle_cast(ignore, State) ->
    NewState = do_ignore(State),
    {noreply, NewState};
handle_cast({update, Realm, Serial, Pid}, State) ->
    NewState = do_update(Realm, Serial, Pid, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tw", [Unexpected]),
    {noreply, State}.


%% @private
%% gen_sever callback for general Erlang message handling

handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Pid, Mon, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tw", [Unexpected]),
    {noreply, State}.


%% @private
%% gen_server callback to handle state transformations necessary for hot
%% code updates. This template performs no transformation.

code_change(_, State, _) ->
    {ok, State}.


%% @private
%% gen_server callback to handle shutdown/cleanup tasks on receipt of a clean
%% termination request.

terminate(_, _) ->
    ok.


%%% Doer Functions

-spec do_listen(State) -> {Response, NewState}
    when State    :: state(),
         Response :: ok
                   | {error, listening},
         NewState :: state().
%% @private
%% The same as do_listen/2 but retrieves and passes the configured default port.

do_listen(State = #s{listener = none}) ->
    SocketOptions =
        [inet6,
         {active,    once},
         {mode,      binary},
         {packet,    4},
         {keepalive, true},
         {reuseaddr, true}],
    {ok, PortNum} = zx_daemon:conf(listen_port),
    {ok, Listener} = gen_tcp:listen(PortNum, SocketOptions),
    {ok, _} = zomp_client:start(Listener),
    {ok, State#s{listener = Listener}};
do_listen(State) ->
    {{error, listening}, State}.


-spec do_ignore(State) -> NewState
    when State    :: state(),
         NewState :: state().
%% @private
%% Closes the listening port, automatically terminating all waiters gracefully.

do_ignore(State = #s{listener = none}) ->
    State;
do_ignore(State = #s{listener = Listener}) ->
    ok = gen_tcp:close(Listener),
    State#s{listener = none}.


-spec do_update(Realm, Serial, Pid, State) -> NewState
    when Realm    :: zx:realm(),
         Serial   :: zx:serial(),
         Pid      :: pid(),
         State    :: state(),
         NewState :: state().
%% TODO: Have leaves subscribe/engage/enroll in a realm instead of broadcasting
%%       updates to all leaves.

do_update(Realm,
          Serial,
          Pid,
          State = #s{realms = Realms, leaf = #clients{pids = Pids}}) ->
    {value, R, NextRealms} = lists:keytake(Realm, #realm.name, Realms),
    NewRealms = [R#realm{serial = Serial, pid = Pid} | NextRealms],
    Broadcast = fun(L) -> zomp_client:update(L, Realm, Serial) end,
    ok = lists:foreach(Broadcast, Pids),
    State#s{realms = NewRealms}.


-spec do_connected(Type, Client, State) -> {Response, NewState}
    when Type      :: node | leaf,
         Client    :: {pid(), none | zx:host(), [zx:realm()]},
         State     :: state(),
         Response  :: {accept, Available}
                    | {redirect, Next},
         Available :: {zx:realm(), zx:serial(), pid()},
         Next      :: [{zx:host(), [zx:realm()]}],
         NewState  :: state().
%% @private
%% Determine whether a new user connection should be accepted or redirected.

do_connected(Type, Client, State = #s{dist = Dist}) when map_size(Dist) == 0 ->
    accept(Type, Client, State);
do_connected(Type, Client = {Pid, Host, _}, State = #s{dist = Dist}) ->
    case maybe_accept(Type, State) of
        accept ->
            accept(Type, Client, State);
        redirect ->
            HS = host_string(Host),
            ok = log(info, "Redirecting ~w client ~p ~ts", [Type, Pid, HS]),
            Next = maps:values(Dist),
            {{redirect, Next}, State}
    end.

host_string(none) -> "unknown";
host_string(Host) -> zx_net:host_string(Host).


maybe_accept(leaf, #s{leaf = #clients{max = M, count = C}}) when M > C ->
    accept;
maybe_accept(vamp, #s{vamp = #clients{max = M, count = C}}) when M > C ->
    accept;
maybe_accept(vamp, #s{node = #clients{max = M, count = C}}) when M > C ->
    accept;
maybe_accept(_, _) ->
    redirect.


accept(leaf,
       {Pid, Host, _},
       State = #s{realms = Realms, mons = Mons, leaf = Data}) ->
    {NewMons, NewData, Available} = accept(leaf, Pid, Host, Mons, Realms, Data),
    {{accept, Available}, State#s{mons = NewMons, leaf = NewData}};
accept(vamp,
       {Pid, Host, _},
       State = #s{realms = Realms, mons = Mons, vamp = Data}) ->
    {NewMons, NewData, Available} = accept(vamp, Pid, Host, Mons, Realms, Data),
    {{accept, Available}, State#s{mons = NewMons, vamp = NewData}};
accept(node,
       {Pid, Host, _},
       State = #s{realms = Realms, mons = Mons, node = Data, dist = Dist}) ->
    {NewMons, NewData, Available} = accept(node, Pid, Host, Mons, Realms, Data),
    NewDist = maps:put(Pid, Host, Dist),
    {{accept, Available}, State#s{mons = NewMons, node = NewData, dist = NewDist}}.


accept(Type, Pid, Host, Mons, Realms, Data = #clients{pids = Pids, count = Count}) ->
    HS = host_string(Host),
    ok = log(info, "Accepting ~w client ~p ~ts", [Type, Pid, HS]),
    Mon = monitor(process, Pid),
    NewMons = maps:put(Pid, {Mon, Type}, Mons),
    NewData = Data#clients{pids = [Pid | Pids], count = Count + 1},
    Available = [{N, S, P} || #realm{name = N, serial = S, pid = P} <- Realms],
    {NewMons, NewData, Available}.


-spec handle_down(Pid, Mon, Info, State) -> NewState
    when Pid      :: pid(),
         Mon      :: reference(),
         Info     :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% Stop tracking closed connection.

handle_down(Pid, Mon, Info, State = #s{mons = Mons}) ->
    case maps:take(Pid, Mons) of
        {{Mon, Type}, NewMons} ->
            scrub(Type, Pid, State#s{mons = NewMons});
        error ->
            Unexpected = {'DOWN', Mon, process, Pid, Info},
            ok = log(warning, "Unexpected info: ~w", [Unexpected]),
            State
    end.


scrub(leaf, Pid, State = #s{leaf = Leaf}) ->
    State#s{leaf = scrub(Pid, Leaf)};
scrub(vamp, Pid, State = #s{vamp = Vamp}) ->
    State#s{vamp = scrub(Pid, Vamp)};
scrub(node, Pid, State = #s{node = Node, dist = Dist}) ->
    State#s{node = scrub(Pid, Node), dist = maps:remove(Pid, Dist)}.

scrub(Pid, Data = #clients{pids = Pids, count = Count}) ->
    Data#clients{pids = lists:delete(Pid, Pids), count = Count - 1}.
