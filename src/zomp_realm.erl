%%% @doc
%%% Zomp Realm
%%%
%%% Realm processes are state workers. Each realm has a separate realm process that
%%% maintains the state of the given realm, and answers queries, maintains subscription
%%% lists, and enforces a sequence on events that affect its realm relative to the
%%% local node, and makes state changes appear atomic to external processes.
%%% @end

-module(zomp_realm).
-vsn("0.6.3").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

% LEAF Interface
-export([list_sysops/1,
         list/1,       list/2,
         list_type/2,
         latest/1,     latest/2,
         provides/2,   list_deps/2,
         tags/2,       search/2,
         describe/2,
         subscribe/2,  unsubscribe/2]).

% NODE Interface
-export([enroll/1, disenroll/1, cache/3,
         serial/2, pull_event/2, pull_span/2, pull_history/1]).

% AUTH Interface
-export([null/4, clear_ss/1,
         list_users/1, 
         list_packagers/2,
         list_maintainers/2,
         add_packager/2,    rem_packager/2,
         add_maintainer/2,  rem_maintainer/2,
         list_pending/2,
         list_approved/1,
         prep_submit/4,     submit/3,
         review/2,
         approve/2,
         reject/2,
         prep_accept/4,     accept/3,
         add_package/2,
         add_user/2, list_user_keys/2, add_user_keys/2]).

% Upstream Interface
-export([upstream/2, update/2]).

% General utilities
-export([serial/1,
         sysops/1,
         fetch/2,
         keychain/2]).

% zomp_realm_man Interface
-export([start_monitor/2]).

% zomp_realm_sup Interface
-export([start_link/2]).

% gen_server Callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-include("$zx_include/zx_logger.hrl").


-record(s,
        {realm    = none       :: none | zx:realm(),
         upstream = none       :: none | prime | {pid(), reference()},
         serial   = 0          :: non_neg_integer(),
         packages = #{}        :: package_index(),
         versions = #{}        :: version_index(),
         modules  = #{}        :: module_index(),
         tags     = #{}        :: tag_index(),
         types    = types()    :: types(),
         approved = []         :: [{zx:name(), zx:version()}],
         sysops   = #{}        :: sysop_index(),
         users    = #{}        :: user_index(),
         keys     = #{}        :: key_index(),
         history  = #{}        :: #{zx:serial() := binary()},
         enrolled = sets:new() :: sets:set(pid()),
         subs     = #{}        :: #{zx:name() := sets:set(pid())},
         mons     = #{}        :: #{pid() := client_status()},
         ss       = none       :: ss_meta()}).

-record(package,
        {maintainers = []   :: [zx:user_name()],
         packagers   = []   :: [zx:user_name()],
         versions    = []   :: [zx:version()],
         pending     = []   :: [zx:version()],
         serial      = none :: none | zx:serial(),
         signatory   = none :: none | zx:user_name(),
         timestamp   = none :: none | calendar:datetime()}).

-record(version,
        {meta        = zx_zsp:new_meta() :: zx_zsp:meta(),
         dependents  = []                :: [zx:package_id()],
         size        = none              :: none | pos_integer(),
         serial      = none              :: none | zx:serial(),
         signatory   = none              :: none | zx:user_name(),
         timestamp   = none              :: none | calendar:datetime()}).

-record(types,
        {gui = [] :: [zx:package_id()],
         cli = [] :: [zx:package_id()],
         app = [] :: [zx:package_id()],
         lib = [] :: [zx:package_id()]}).

-record(sysop,
        {serial      = none :: none | zx:serial(),
         signatory   = none :: none | zx:user_name(),
         timestamp   = none :: none | calendar:datetime()}).

-record(user,
        {keys        = []   :: [zx:key_name()],
         packaging   = []   :: [zx:name()],
         maintaining = []   :: [zx:name()],
         contact     = []   :: [{Method :: string(), Info :: string()}],
         name        = none :: none | string(),
         serial      = none :: none | zx:serial(),
         signatory   = none :: none | zx:user_name(),
         timestamp   = none :: none | calendar:datetime()}).

-record(key,
        {owner       = none :: none | zx:user_name(),
         chain_sig   = none :: none | root | {zx:key_name(), binary()},
         serial      = none :: none | zx:serial(),
         signatory   = none :: none | zx:user_name(),
         timestamp   = none :: none | calendar:datetime()}).

-record(ss,
        {serial    :: zx:serial(),
         signatory :: zx:user_name(),
         keyname   :: zx:key_name(),
         timestamp :: erlang:timestamp(),
         action    :: ss_action(),
         pid       :: pid(),
         mon       :: reference()}).

-type state()         :: #s{}.
-type client_status() :: {Monitor       :: reference(),
                          Enrolled      :: boolean(),
                          Subscriptions :: sets:set(zx:name())}.
-type ss_meta()       :: #ss{}.
-type ss_action()     :: null
                       | {accept,
                          Package :: {zx:name(), zx:version()},
                          Meta    :: zsp:meta(),
                          Message :: binary(),
                          ReqMeta :: req_meta()}.
-type ss_message()    :: {Message :: binary(),
                          {Serial    :: zx:serial(),
                           Timestamp :: erlang:timestamp(),
                           Signatory :: zx:user_name(),
                           Target    :: term()}}.
-type su_message()    :: {Sig    :: binary(),
                          Signed :: binary(),
                          {Signatory :: zx:user_name(),
                           KeyName   :: zx:key_name(),
                           Target    :: term()}}.

-type accept_meta()   :: {Tag       :: zx:ss_tag(),
                          Signatory :: zx:user_name(),
                          KeyName   :: zx:key_name(),
                          ReqMeta   :: req_meta()}.

-type req_meta()      :: {Realm     :: zx:realm(),
                          Package   :: zx:package(),
                          Meta      :: zx_zsp:meta(),
                          Size      :: pos_integer()}.

-type package_index() :: #{zx:name()                 := package_data()}.
-type version_index() :: #{{zx:name(), zx:version()} := version_data()}.
-type module_index()  :: #{string()                  := sets:set(zx:package_id())}.
-type tag_index()     :: #{string()                  := #{zx:name() := [zx:version()]}}.
-type sysop_index()   :: #{zx:user_name()            := sysop_data()}.
-type user_index()    :: #{zx:user_name()            := user_data()}.
-type key_index()     :: #{zx:key_name()             := key_data()}.

-type package_data()  :: #package{}.
-type version_data()  :: #version{}.
-type types()         :: #types{}.
-type sysop_data()    :: #sysop{}.
-type user_data()     :: #user{}.
-type key_data()      :: #key{}.



%%% LEAF Interface


-spec list_sysops(Realm) -> Result
    when Realm  :: pid(),
         Result :: {ok, [{zx:user_name(), zx:real_name(), [zx:contact_info()]}]}.

list_sysops(Realm) ->
    gen_server:call(Realm, list_sysops).


-spec list(Realm) -> Result
    when Realm  :: pid(),
         Result :: [zx:package()].

list(Realm) -> gen_server:call(Realm, list).


-spec list(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()}
                  | zx:name(),
         Result  :: {ok, [zx:package_id()]}
                  | {error, bad_package}.

list(Realm, {Name, Version}) ->
    gen_server:call(Realm, {list, Name, Version});
list(Realm, Name) ->
    gen_server:call(Realm, {list, Name}).


-spec list_type(Realm, Type) -> Result
    when Realm  :: pid(),
         Type   :: zx:package_type(),
         Result :: {ok, [zx:pacage_id()]}
                 | {error, bad_type}.

list_type(Realm, Type)
        when Type =:= gui;
             Type =:= cli;
             Type =:= app;
             Type =:= lib ->
    gen_server:call(Realm, {list_type, Type});
list_type(_, _) ->
    {error, bad_type}.


-spec latest(Realm :: pid()) -> {ok, zx:serial()}.

latest(Realm) ->
    gen_server:call(Realm, latest).


-spec latest(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()}
                  | zx:name(),
         Result  :: {ok, [zx:package_id()]}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version.

latest(Realm, {Name, Version}) ->
    gen_server:call(Realm, {latest, Name, Version});
latest(Realm, Name) ->
    gen_server:call(Realm, {latest, Name}).


-spec provides(Realm, Module) -> {ok, [zx:package_id()]}
    when Realm  :: pid(),
         Module :: string().

provides(Realm, Module) ->
    gen_server:call(Realm, {provides, Module}).


-spec list_deps(Realm, PV) -> Result
    when Realm  :: pid(),
         PV     :: {zx:name(), zx:version()},
         Result :: {ok, [zx:package_id()]}
                 | {error, bad_package | bad_version}.

list_deps(Realm, PV) ->
    gen_server:call(Realm, {list_deps, PV}).


-spec tags(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()},
         Result  :: {ok, [zx:search_tag()]}
                  | {error, bad_package}.

tags(Realm, Package) ->
    gen_server:call(Realm, {tags, Package}).


-spec search(Realm, String) -> Result
    when Realm  :: pid(),
         String :: string(),
         Result :: {ok, [{zx:name(), [zx:version()]}]}.

search(Realm, String) ->
    gen_server:call(Realm, {search, String}).


-spec describe(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()},
         Result  :: {ok, zx:description()}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version.

describe(Realm, Package) ->
    gen_server:call(Realm, {describe, Package}).


-spec subscribe(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: zx:name(),
         Result  :: {ok, zx:serial()}
                  | {error, bad_package}.
%% @doc
%% Subscribe to updates about a specific package.

subscribe(Realm, Package) ->
    gen_server:call(Realm, {subscribe, Package}).


-spec unsubscribe(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: zx:name(),
         Result  :: {ok, zx:serial()}
                  | {error, bad_package}.
%% @doc
%% Unsubscribe to updates about a specific package. It is not an error to unsubscribe
%% from a package not subscribed.

unsubscribe(Realm, Package) ->
    gen_server:cast(Realm, {unsubscribe, self(), Package}).



%%% NODE Interface

-spec enroll(Realm :: pid()) -> {ok, zx:serial()}.
%% @doc
%% Enroll as a downstream distributor.

enroll(Realm) ->
    gen_server:call(Realm, enroll).


-spec disenroll(Realm :: pid()) -> ok.
%% @doc
%% Disenroll as a downstream distributor.

disenroll(Realm) ->
    gen_server:cast(Realm, {disenroll, self()}).


-spec cache(Realm, Package, Bin) -> Outcome
    when Realm   :: zx:realm(),
         Package :: {zx:name(), zx:version()},
         Bin     :: binary(),
         Outcome :: ok
                  | {error, Reason :: term()}.

cache(Realm, Package, Bin) ->
    gen_server:call(Realm, {cache, Package, Bin}).


-spec serial(Realm, Serial) -> ok
    when Realm  :: zx:realm(),
         Serial :: zx:serial().
%% @private
%% Notify the realm of the most recent upstream serial received so it can adjust if
%% necessary.

serial(Realm, Serial) ->
    gen_server:cast(Realm, {serial, Serial}).


-spec pull_event(Realm, Serial) -> ok
    when Realm  :: pid(),
         Serial :: zx:serial().

pull_event(Realm, Serial) ->
    gen_server:cast(Realm, {pull_event, Serial, self()}).


-spec pull_span(Realm, Range) -> ok
    when Realm  :: pid(),
         Range  :: {zx:serial(), zx:serial()}.

pull_span(Realm, {Start, Stop}) when Start =< Stop ->
    gen_server:cast(Realm, {pull_span, Start, Stop, self()}).


-spec pull_history(Realm :: pid()) -> {ok, History :: map()}.

pull_history(Realm) ->
    gen_server:call(Realm, pull_history).



%%% AUTH Interface

-spec null(Realm, TS, Sysop, KeyName) -> Result
    when Realm   :: pid(),
         TS      :: erlang:timestamp(),
         Sysop   :: zx:user_name(),
         KeyName :: zx:key_name(),
         Result  :: {ok, zx:ss_tag(), public_key:rsa_public_key()}
                  | {error, Reason},
         Reason  :: busy
                  | no_permission
                  | not_prime.
%% @doc
%% Initiate an SS request sequence.

null(Realm, TS, Sysop, KeyName) ->
    gen_server:call(Realm, {null, TS, Sysop, KeyName}).


-spec clear_ss(Realm :: pid()) -> ok.

clear_ss(Realm) ->
    gen_server:cast(Realm, clear_ss).


-spec list_users(Realm) -> Result
    when Realm   :: pid(),
         Result  :: {ok, Users :: [zx:user_name()]}
                  | {error, not_prime}.

list_users(Realm) ->
    gen_server:call(Realm, list_users).


-spec list_packagers(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: zx:name(),
         Result  :: {ok, Packagers :: [zx:user_name()]}
                  | {error, Reason},
         Reason  :: bad_package
                  | not_prime.

list_packagers(Realm, Package) ->
    gen_server:call(Realm, {list_packagers, Package}).


-spec list_maintainers(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: zx:name(),
         Result  :: {ok, Maintainers :: [zx:user_name()]}
                  | {error, Reason},
         Reason  :: bad_package
                  | not_prime.

list_maintainers(Realm, Package) ->
    gen_server:call(Realm, {list_maintainers, Package}).


-spec add_packager(Realm, Data) -> Result
    when Realm    :: pid(),
         Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package  :: zx:name(),
                        Packager :: zx:user_name()}}},
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package.

add_packager(Realm, Data) ->
    gen_server:call(Realm, {add_packager, Data}).


-spec rem_packager(Realm, Data) -> Result
    when Realm    :: pid(),
         Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package  :: zx:name(),
                        Packager :: zx:user_name()}}},
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package.

rem_packager(Realm, Data) ->
    gen_server:call(Realm, {rem_packager, Data}).


-spec add_maintainer(Realm, Data) -> Result
    when Realm    :: pid(),
         Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package    :: zx:name(),
                        Maintainer :: zx:user_name()}}},
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package.

add_maintainer(Realm, Data) ->
    gen_server:call(Realm, {add_maintainer, Data}).


-spec rem_maintainer(Realm, Data) -> Result
    when Realm    :: pid(),
         Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package    :: zx:name(),
                        Maintainer :: zx:user_name()}}},
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package.

rem_maintainer(Realm, Data) ->
    gen_server:call(Realm, {rem_maintainer, Data}).


-spec list_pending(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: zx:name(),
         Result  :: {ok, [zx:version()]}
                  | {error, Reason},
         Reason  :: bad_package
                  | not_prime.

list_pending(Realm, Package) ->
    gen_server:call(Realm, {list_pending, Package}).


-spec list_approved(Realm) -> Result
    when Realm  :: pid(),
         Result :: {ok, [{zx:name(), zx:version()}]}
                 | {error, not_prime}.

list_approved(Realm) ->
    gen_server:call(Realm, list_approved).


-spec prep_submit(Realm, Submitter, KeyName, Package) -> Result
    when Realm     :: pid(),
         Submitter :: zx:user_name(),
         KeyName   :: zx:key_name(),
         Package   :: zx:name(),
         Result    :: {ok, PublicKey :: term()}
                    | {error, Reason},
         Reason    :: bad_key
                    | bad_user
                    | bad_package
                    | unknown_user
                    | no_permission.

prep_submit(Realm, Submitter, KeyName, Package) ->
    gen_server:call(Realm, {prep_submit, Submitter, KeyName, Package}).


-spec submit(Realm, Package, Bin) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()},
         Bin     :: binary(),
         Result  :: ok | {error, Reason},
         Reason  :: bad_package
                  | bad_version
                  | not_prime.
%% @doc
%% Add a package to the review queue and store it.

submit(Realm, {Name, Version}, Bin) ->
    gen_server:call(Realm, {submit, Name, Version, Bin}).


-spec review(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()},
         Result  :: {ok, binary()}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version
                  | not_prime
                  | {load_fail, string()}.

review(Realm, {Name, Version}) ->
    gen_server:call(Realm, {review, Name, Version}).


-spec approve(Realm, Message) -> Result
    when Realm   :: pid(),
         Message :: su_message(),
         Result  :: ok
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version
                  | not_prime.

approve(Realm, Message) ->
    gen_server:call(Realm, {approve, Message}).


-spec reject(Realm, Message) -> Result
    when Realm   :: pid(),
         Message :: su_message(),
         Result  :: ok
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version
                  | not_prime
                  | bad_user
                  | bad_key
                  | bad_sig.

reject(Realm, Message) ->
    gen_server:call(Realm, {reject, Message}).


-spec prep_accept(Realm, Package, MessageBin, AcceptMeta) -> Result
    when Realm      :: pid(),
         Package    :: {zx:name(), zx:version()},
         MessageBin :: binary(),
         AcceptMeta :: accept_meta(),
         Result     :: ok
                     | {error, Reason},
         Reason     :: not_found
                     | bad_request
                     | unknown_user
                     | no_permission
                     | bad_sig
                     | bad_key
                     | not_prime.

prep_accept(Realm, Package, MessageBin, AcceptMeta) ->
    gen_server:call(Realm, {prep_accept, Package, MessageBin, AcceptMeta}).


-spec accept(Realm, Tag, ZspBin) -> Result
    when Realm  :: pid(),
         Tag    :: zx:ss_tag(),
         ZspBin :: binary(),
         Result :: ok
                 | {error, Reason},
         Reason :: bad_sig
                 | bad_request
                 | not_prime.

accept(Realm, Tag, ZspBin) ->
    gen_server:call(Realm, {accept, Tag, ZspBin}).


-spec add_package(pid(), ss_message()) -> ok | {error, Reason}
    when Reason :: exists | bad_package | bad_request | not_prime.

add_package(Realm, SSMessage) ->
    gen_server:call(Realm, {ss, fun do_add_package/2, SSMessage}).


-spec add_user(Realm, Message) -> ok | {error, Reason}
    when Realm   :: pid(),
         Message :: su_message(),
         Reason  :: exists
                  | {bad_key, zx:key_name()}
                  | bad_request
                  | not_prime
                  | bad_auth
                  | bad_user.

add_user(Realm, Message) ->
    gen_server:call(Realm, {add_user, Message}).


-spec list_user_keys(Realm, UserName) -> {ok, [zx:key_name()]} | {error, bad_user}
    when Realm    :: zx:realm(),
         UserName :: zx:user_name().

list_user_keys(Realm, UserName) ->
    gen_server:call(Realm, {list_user_keys, UserName}).


-spec add_user_keys(Realm, Message) -> ok | {error, Reason}
    when Realm    :: pid(),
         Message  :: su_message(),
         Reason   :: bad_user
                   | {bad_sig, zx:key_name()}
                   | {bad_key, zx:key_name()}.

add_user_keys(Realm, Message) ->
    gen_server:call(Realm, {add_user_keys, Message}).


-spec upstream(Realm, Conn) -> ok
    when Realm :: pid(),
         Conn  :: pid().

upstream(Realm, Conn) ->
    gen_server:cast(Realm, {upstream, Conn}).


-spec update(Realm, Message) -> ok
    when Realm   :: pid(),
         Message :: binary().

update(Realm, Message) ->
    gen_server:cast(Realm, {update, Message}).


%% General Utilities

-spec serial(Realm) -> {ok, zx:serial()}
    when Realm :: pid().

serial(Realm) ->
    gen_server:call(Realm, serial).

-spec sysops(Realm) -> Result
    when Realm  :: pid(),
         Result :: {ok, [zx:user_name()]}.

sysops(Realm) ->
    gen_server:call(Realm, sysops).


-spec fetch(Realm, Package) -> Result
    when Realm   :: pid(),
         Package :: {zx:name(), zx:version()},
         Result  :: {hops, pos_integer()}
                  | {ok, binary}
                  | {error, Reason :: term()}.

fetch(Realm, Package) ->
    gen_server:call(Realm, {fetch, Package}).


-spec keychain(Realm, KeyName) -> Result
    when Realm   :: pid(),
         KeyName :: zx:key_name(),
         Result  :: {ok, [zx:key_data()]}
                  | {error, Reason},
         Reason  :: bad_user
                  | bad_realm
                  | no_pub
                  | bad_key.
%% @private

keychain(Realm, KeyName) ->
    gen_server:call(Realm, {keychain, KeyName}).


%%% Startup

-spec start_monitor(Realm, Role) -> Result
    when Realm  :: zx:realm(),
         Role   :: prime | distributor,
         Result :: {ok, reference(), pid()}.

start_monitor(Realm, Role) ->
    {ok, Pid} = zomp_realm_sup:start_realm(Realm, Role),
    Mon = monitor(process, Pid),
    {ok, Mon, Pid}.


-spec start_link(Realm, Role) -> Result
    when Realm  :: zx:realm(),
         Role   :: prime | distributor,
         Result :: {ok, pid()}.

start_link(Realm, Role) ->
    gen_server:start_link(?MODULE, {Realm, Role}, []).


-spec init({Realm, Role}) -> Result
    when Realm  :: zx:realm(),
         Role   :: prime | distributor,
         Result :: {ok, state()}.

init({Realm, Role}) ->
    ok = tell("Starting as ~ts ~tp.", [Realm, Role]),
    ok = ensure_dirs(Realm),
    State =
        case Role of
            prime       -> #s{realm = Realm, upstream = prime};
            distributor -> #s{realm = Realm}
        end,
    case maybe_load_stash(State) of
        {ok, NewState = #s{serial = Serial}} ->
            ok = zomp_client_man:update(Realm, Serial),
            {ok, NewState};
        Problem ->
            Problem
    end.


%%% gen_server Message Handling Callbacks

handle_call(list_sysops, _, State) ->
    Result = do_list_sysops(State),
    {reply, Result, State};
handle_call(list, _, State) ->
    Result = do_list(State),
    {reply, Result, State};
handle_call({list, Name}, _, State) ->
    Result = do_list(Name, State),
    {reply, Result, State};
handle_call({list, Name, Version}, _, State) ->
    Result = do_list(Name, Version, State),
    {reply, Result, State};
handle_call({list_type, Type}, _, State) ->
    Result = do_list_type(Type, State),
    {reply, Result, State};
handle_call(latest, _, State) ->
    Result = do_latest(State),
    {reply, Result, State};
handle_call({latest, Name}, _, State) ->
    Result = do_latest(Name, State),
    {reply, Result, State};
handle_call({latest, Name, Version}, _, State) ->
    Result = do_latest(Name, Version, State),
    {reply, Result, State};
handle_call({provides, Module}, _, State) ->
    Result = do_provides(Module, State),
    {reply, Result, State};
handle_call({list_deps, PV}, _, State) ->
    Result = do_list_deps(PV, State),
    {reply, Result, State};
handle_call({tags, Package}, _, State) ->
    Result = do_tags(Package, State),
    {reply, Result, State};
handle_call({search, String}, _, State) ->
    Result = do_search(String, State),
    {reply, Result, State};
handle_call({describe, Package}, _, State) ->
    Result = do_describe(Package, State),
    {reply, Result, State};
handle_call({subscribe, Package}, {Requestor, _}, State) ->
    {Result, NewState} = do_subscribe(Requestor, Package, State),
    {reply, Result, NewState};
handle_call(enroll, {Pid, _}, State) ->
    {Result, NewState} = do_enroll(Pid, State),
    {reply, Result, NewState};
handle_call(pull_history, _, State = #s{history = History}) ->
    Result = {ok, History},
    {reply, Result, State};
handle_call({null, TS, Sysop, KeyName}, {Pid, _}, State) ->
    {Result, NewState} = do_null(Pid, TS, Sysop, KeyName, State),
    {reply, Result, NewState};
handle_call(list_users, _, State) ->
    Result = do_list_users(State),
    {reply, Result, State};
handle_call({list_packagers, Package}, _, State) ->
    Result = do_list_packagers(Package, State),
    {reply, Result, State};
handle_call({list_maintainers, Package}, _, State) ->
    Result = do_list_maintainers(Package, State),
    {reply, Result, State};
handle_call({add_packager, Data}, _, State) ->
    {Result, NewState} = do_add_packager(Data, State),
    {reply, Result, NewState};
handle_call({rem_packager, Data}, _, State) ->
    {Result, NewState} = do_rem_packager(Data, State),
    {reply, Result, NewState};
handle_call({add_maintainer, Data}, _, State) ->
    {Result, NewState} = do_add_maintainer(Data, State),
    {reply, Result, NewState};
handle_call({rem_maintainer, Data}, _, State) ->
    {Result, NewState} = do_rem_maintainer(Data, State),
    {reply, Result, NewState};
handle_call({list_pending, Package}, _, State) ->
    Result = do_list_pending(Package, State),
    {reply, Result, State};
handle_call(list_approved, _, State) ->
    Result = do_list_approved(State),
    {reply, Result, State};
handle_call({prep_submit, Submitter, KeyName, Package}, _, State) ->
    {Result, NewState} = do_prep_submit(Submitter, KeyName, Package, State),
    {reply, Result, NewState};
handle_call({submit, Name, Version, Bin}, _, State) ->
    {Result, NewState} = do_submit(Name, Version, Bin, State),
    {reply, Result, NewState};
handle_call({review, Name, Version}, _, State) ->
    {Result, NewState} = do_review(Name, Version, State),
    {reply, Result, NewState};
handle_call({approve, Message}, _, State) ->
    {Result, NewState} = do_approve(Message, State),
    {reply, Result, NewState};
handle_call({reject, Message}, _, State) ->
    {Result, NewState} = do_reject(Message, State),
    {reply, Result, NewState};
handle_call({prep_accept, Package, MessageBin, AcceptMeta}, _, State) ->
    {Result, NewState} = do_prep_accept(Package, MessageBin, AcceptMeta, State),
    {reply, Result, NewState};
handle_call({accept, Tag, PackageBin}, {Pid, _}, State) ->
    {Result, NewState} = do_accept(Pid, Tag, PackageBin, State),
    {reply, Result, NewState};
handle_call({ss, Action, SSData}, {Pid, _}, State) ->
    {Result, NewState} = handle_ss(Action, SSData, Pid, State),
    {reply, Result, NewState};
handle_call({add_user, Message}, _, State) ->
    {Result, NewState} = do_add_user(Message, State),
    {reply, Result, NewState};
handle_call({list_user_keys, UserName}, _, State) ->
    Result = do_list_user_keys(UserName, State),
    {reply, Result, State};
handle_call({add_user_keys, Message}, _, State) ->
    {Result, NewState} = do_add_user_keys(Message, State),
    {reply, Result, NewState};
handle_call(serial, _, State = #s{serial = Serial}) ->
    Result = {ok, Serial},
    {reply, Result, State};
handle_call(sysops, _, State = #s{sysops = Sysops}) ->
    Result = {ok, maps:keys(Sysops)},
    {reply, Result, State};
handle_call({fetch, PackageID}, _, State) ->
    Result = do_fetch(PackageID, State),
    {reply, Result, State};
handle_call({cache, Package, Bin}, _, State) ->
    Result = do_cache(Package, Bin, State),
    {reply, Result, State};
handle_call({keychain, KeyName}, _, State) ->
    Result = do_keychain(KeyName, State),
    {reply, Result, State};
handle_call(Huh, From, State) ->
    ok = log(warning, "Unexpected call from ~tw: ~tw~n", [From, Huh]),
    {noreply, State}.


handle_cast({pull_event, Serial, Node}, State) ->
    ok = do_pull_event(Serial, Node, State),
    {noreply, State};
handle_cast({pull_span, Start, Stop, Node}, State) ->
    ok = do_pull_span(Start, Stop, Node, State),
    {noreply, State};
handle_cast({unsubscribe, Requestor, Package}, State) ->
    NewState = do_unsubscribe(Requestor, Package, State),
    {noreply, NewState};
handle_cast({serial, Serial}, State) ->
    ok = sync_history(Serial, State),
    {noreply, State};
handle_cast({disenroll, Pid}, State) ->
    NewState = do_disenroll(Pid, State),
    {noreply, NewState};
handle_cast(clear_ss, State) ->
    NewState = do_clear_ss(State),
    {noreply, NewState};
handle_cast({upstream, Conn}, State) ->
    NewState = do_upstream(Conn, State),
    {noreply, NewState};
handle_cast({update, Message}, State) ->
    NewState = do_update(Message, State),
    {noreply, NewState};
handle_cast(Huh, State) ->
    ok = log(warning, "Unexpected cast: ~tw~n", [Huh]),
    {noreply, State}.


handle_info({'DOWN', Mon, process, Pid, Info}, State) ->
    NewState = handle_down(Mon, Pid, Info, State),
    {noreply, NewState};
handle_info(Huh, State) ->
    ok = log(warning, "Unexpected info: ~tw~n", [Huh]),
    {noreply, State}.


-spec handle_ss(Action, {MessageBin, Data}, Pid, State) -> {Result, NewState}
    when Action     :: fun(),
         MessageBin :: binary(),
         Data       :: {zx:serial(), erlang:timestamp(), Target :: term()},
         Pid        :: pid(),
         State      :: state(),
         Result     :: ok | {error, Reason :: term()},
         NewState   :: state().

handle_ss(Action,
          {MessageBin, {{Serial, Timestamp}, Signatory, KeyName, Target}},
          Pid,
          State = #s{upstream = prime,
                     ss       = #ss{serial    = Serial,
                                    timestamp = Timestamp,
                                    action    = null,
                                    signatory = Signatory,
                                    keyname   = KeyName,
                                    pid       = Pid,
                                    mon       = Mon}}) ->
    {Result, NewState} =
        case Action(Target, State) of
            {ok, State2 = #s{realm = Realm, history = History, enrolled = Enrolled}} ->
                NewHistory = maps:put(Serial, MessageBin, History),
                State3 = State2#s{history = NewHistory},
                ok = write_history(Realm, NewHistory),
                ok = stash(State3),
                ok = zomp_client_man:update(Realm, Serial),
                ok = broadcast_ss(MessageBin, sets:to_list(Enrolled)),
                {ok, State3};
            Error ->
                {Error, State}
        end,
    true = demonitor(Mon),
    {Result, NewState#s{ss = none}};
handle_ss(_, _, _, State = #s{upstream = prime}) ->
    {{error, bad_request}, State};
handle_ss(_, _, _, State) ->
    {{error, not_prime}, State}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
%% @private
%% The gen_server:code_change/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
%% @private
%% The gen_server:terminate/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2

terminate(shutdown, State) -> stash(State);
terminate(normal,   State) -> stash(State);
terminate(Reason,   State) -> log(error, "Terminate: ~tw; State: ~tw", [Reason, State]).



%%% Doer Functions

-spec do_list(state()) -> {ok, [zx:package()]}.

do_list(#s{packages = Packages}) ->
    Names = maps:keys(Packages),
    {ok, Names}.


-spec do_list(Name, State) -> Result
    when Name    :: zx:name(),
         State   :: state(),
         Result  :: {ok, [zx:version()]}
                  | {error, bad_package}.

do_list(Name, State) ->
    do_list(Name, {z, z, z}, State).


-spec do_list(Name, Version, State) -> Result
    when Name    :: zx:name(),
         Version :: zx:version(),
         State   :: state(),
         Result  :: {ok, [zx:version()]}
                  | {error, bad_package}.

do_list(Name, Version, #s{packages = Packages}) ->
    case maps:find(Name, Packages) of
        {ok, Package} -> do_list2(Version, Package);
        error         -> {error, bad_package}
    end.

do_list2({z, z, z}, #package{versions = Versions}) ->
    {ok, Versions};
do_list2({X, z, z}, #package{versions = Versions}) ->
    {ok, [V || V = {Q, _, _} <- Versions, Q == X]};
do_list2({X, Y, z}, #package{versions = Versions}) ->
    {ok, [V || V = {Q, W, _} <- Versions, Q == X, W == Y]};
do_list2(Version, #package{versions = Versions}) ->
    case lists:member(Version, Versions) of
        true  -> {ok, [Version]};
        false -> {ok, []}
    end.


-spec do_list_type(Type, State) -> Result
    when Type   :: zx:package_type(),
         State  :: state(),
         Result :: {ok, [zx:package_id()]}.

do_list_type(gui, #s{types = #types{gui = GUIs}}) ->
    {ok, GUIs};
do_list_type(cli, #s{types = #types{cli = CLIs}}) ->
    {ok, CLIs};
do_list_type(app, #s{types = #types{app = Apps}}) ->
    {ok, Apps};
do_list_type(lib, #s{types = #types{lib = Libs}}) ->
    {ok, Libs}.


-spec do_latest(state()) -> {ok, zx:serial()}.

do_latest(#s{serial = Serial}) ->
    {ok, Serial}.


-spec do_latest(Name, State) -> Result
    when Name    :: zx:name(),
         State   :: state(),
         Result  :: {ok, zx:version()}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version.

do_latest(Name, State) ->
    do_latest(Name, {z, z, z}, State).


-spec do_latest(Name, Version, State) -> Result
    when Name    :: zx:name(),
         Version :: zx:version(),
         State   :: state(),
         Result  :: {ok, Version}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version.

do_latest(Name, Version, State) ->
    case do_list(Name, Version, State) of
        {ok, []}       -> {error, bad_version};
        {ok, Versions} -> {ok, lists:last(lists:sort(Versions))};
        Error          -> Error
    end.


-spec do_provides(Module, State) -> {ok, [zx:package_id()]}
    when Module :: string(),
         State  :: state().

do_provides(Module, #s{modules = Modules}) ->
    case maps:find(Module, Modules) of
        {ok, PackageSet} -> {ok, sets:to_list(PackageSet)};
        error            -> {ok, []}
    end.


-spec do_list_deps(PV, State) -> Result
    when PV     :: {zx:name(), zx:version()},
         State  :: state(),
         Result :: {ok, [zx:package_id()]}
                 | {error, bad_package | bad_version}.

do_list_deps(PV, #s{packages = Packages, versions = Versions}) ->
    case maps:find(PV, Versions) of
        {ok, #version{meta = #{deps := Deps}}} ->
            {ok, Deps};
        error ->
            Package = element(1, PV),
            case maps:is_key(Package, Packages) of
                true  -> {error, bad_version};
                false -> {error, bad_package}
            end
    end.


-spec do_tags(Package, State) -> Result
    when Package :: {zx:name(), zx:version()},
         State   :: state(),
         Result  :: {ok, [zx:search_tag()]}
                  | {error, bad_package}.

do_tags(Package, #s{versions = Versions}) ->
    case maps:find(Package, Versions) of
        {ok, #version{meta = #{tags := Tags}}} -> {ok, Tags};
        error                                  -> {error, bad_package}
    end.


-spec do_search(String, State) -> Result
    when String :: string(),
         State  :: state(),
         Result :: {ok, [{zx:name(), [zx:version()]}]}.

do_search(String, #s{tags = Tags}) ->
    case maps:find(String, Tags) of
        {ok, Packages} -> {ok, maps:to_list(Packages)};
        error          -> {ok, []}
    end.


-spec do_describe(Package, State) -> Result
    when Package :: {zx:name(), zx:version()},
         State   :: state(),
         Result  :: {ok, zx:description()}
                  | {error, bad_package | bad_version}.

do_describe({Name, Version}, State = #s{versions = Versions}) ->
    case do_latest(Name, Version, State) of
        {ok, Latest} -> do_describe2({Name, Latest}, Versions);
        Error        -> Error
    end.

do_describe2(Package, Versions) ->
    case maps:find(Package, Versions) of
        {ok, #version{meta = Meta}} ->
            #{package_id := PackageID,
              name       := DName,
              type       := Type,
              desc       := Desc,
              author     := Author,
              a_email    := AEmail,
              ws_url     := WebURL,
              repo_url   := RepoURL,
              tags       := Tags} = Meta,
            Description =
                {description,
                 PackageID, DName, Type, Desc, Author, AEmail, WebURL, RepoURL, Tags},
            {ok, Description};
        error ->
            Message = "do_latest/2 found ~p but not found in #s.versions.",
            ok = log(error, Message, [Package]),
            {error, bad_version}
    end.


-spec do_subscribe(Pid, Package, State) -> {Result, NewState}
    when Pid      :: pid(),
         Package  :: zx:name(),
         State    :: state(),
         Result   :: {ok, zx:serial()}
                   | {error, bad_package},
         NewState :: state().

do_subscribe(Pid, Package, State = #s{packages = Packages}) ->
    case maps:is_key(Package, Packages) of
        true  -> do_subscribe2(Pid, Package, State);
        false -> {{error, bad_package}, State}
    end.


do_subscribe2(Pid, Package, State = #s{serial = Serial, subs = Subs, mons = Mons}) ->
    Join = fun(Subscribers) -> sets:add_element(Pid, Subscribers) end,
    NewSubs = maps:update_with(Package, Join, sets:from_list([Pid]), Subs),
    NewMons =
        case maps:find(Pid, Mons) of
            {ok, {Mon, Enrolled, Subscriptions}} ->
                NewSubscriptions = sets:add_element(Package, Subscriptions),
                maps:put(Pid, {Mon, Enrolled, NewSubscriptions}, Mons);
            error ->
                Mon = monitor(process, Pid),
                Subscriptions = sets:from_list([Package]),
                maps:put(Pid, {Mon, false, Subscriptions}, Mons)
        end,
    {{ok, Serial}, State = #s{subs = NewSubs, mons = NewMons}}.


-spec do_unsubscribe(Pid, Package, State) -> NewState
    when Pid      :: pid(),
         Package  :: zx:name(),
         State    :: state(),
         NewState :: state().

do_unsubscribe(Pid, Package, State = #s{subs = Subs, mons = Mons}) ->
    case maps:find(Package, Subs) of
        {ok, Subscribers} ->
            NewSubscribers = sets:del_element(Pid, Subscribers),
            NewSubs = maps:put(Package, NewSubscribers, Subs),
            NewMons = scrub_mons(Pid, Package, Mons),
            State#s{subs = NewSubs, mons = NewMons};
        error ->
            State
    end.

scrub_mons(Pid, Package, Mons) ->
    case maps:find(Pid, Mons) of
        {ok, {Mon, true, Subs}} ->
            NewSubs = sets:del_element(Package, Subs),
            maps:put(Pid, {Mon, true, NewSubs}, Mons);
        {ok, {Mon, false, Subs}} ->
            NewSubs = sets:del_element(Package, Subs),
            case sets:is_empty(NewSubs) of
                true ->
                    true = demonitor(Mon, [flush]),
                    maps:delete(Pid, Mons);
                false ->
                    maps:put(Pid, {Mon, false, NewSubs}, Mons)
            end;
        error ->
            Mons
    end.


-spec do_enroll(Pid, State) -> {Result, NewState}
    when Pid      :: pid(),
         State    :: state(),
         Result   :: {ok, zx:serial()},
         NewState :: state().

do_enroll(Pid, State = #s{serial = Serial, enrolled = Enrolled, mons = Mons}) ->
    NewEnrolled = sets:add_element(Pid, Enrolled),
    NewMons =
        case maps:find(Pid, Mons) of
            {ok, Status} ->
                NewStatus = setelement(2, Status, true),
                maps:put(Pid, NewStatus, Mons);
            error ->
                Mon = monitor(process, Pid),
                Status = {Mon, true, sets:new()},
                maps:put(Pid, Status, Mons)
        end,
    {{ok, Serial}, State#s{enrolled = NewEnrolled, mons = NewMons}}.


-spec do_disenroll(Pid, State) -> NewState
    when Pid      :: pid(),
         State    :: state(),
         NewState :: state().

do_disenroll(Pid, State = #s{enrolled = Enrolled, mons = Mons}) ->
    NewEnrolled = sets:del_element(Pid, Enrolled),
    NewMons =
        case maps:take(Pid, Mons) of
            {Status, NextMons} ->
                Reference = element(1, Status),
                true = demonitor(Reference, [flush]),
                NextMons;
            error ->
                Mons
        end,
    State#s{enrolled = NewEnrolled, mons = NewMons}.


-spec do_pull_event(Serial, Node, State) -> ok
    when Serial :: zx:serial(),
         Node   :: pid(),
         State  :: state().

do_pull_event(Serial, Node, #s{history = History}) ->
    case maps:find(Serial, History) of
        {ok, Message} -> zomp_client:forward(Node, Message);
        error         -> ok
    end.


-spec do_pull_span(Start, Stop, Node, State) -> ok
    when Start  :: zx:serial(),
         Stop   :: zx:serial(),
         Node   :: pid(),
         State  :: state().

do_pull_span(Start, Stop, Node, #s{history = History}) when Start =< Stop ->
    case maps:is_key(Start, History) and maps:is_key(Stop, History) of
        true ->
            Segment = maps:with(lists:seq(Start, Stop), History),
            Send = fun(Message) -> zomp_client:forward(Node, Message) end,
            Span = [element(2, E) || E <- lists:sort(maps:to_list(Segment))],
            lists:foreach(Send, Span);
        false ->
            ok
    end.


-spec do_null(Pid, TS, Sysop, KeyName, State) -> {Result, NewState}
    when Pid      :: pid(),
         TS       :: erlang:timestamp(),
         Sysop    :: zx:user_name(),
         KeyName  :: zx:key_name(),
         State    :: state(),
         Result   :: {ok, zx:ss_tag(), public_key:rsa_public_key()}
                   | {error, Reason},
         Reason   :: busy
                   | no_permission
                   | bad_key,
         NewState :: state().
%% @private
%% If there is no SS stored then issue one and stash it.
%% Because a client may have received an SS value and then never completed its
%% operation there is a chance a stale SS could be cached. Stale or invalid SS
%% caches must be checked for, and if found invalid issue a new one, otherwise
%% indicate busy.
%%
%% NOTE:
%% A stale ss_meta that is never reaped could occur due to a failure in a client
%% that is preparing a pending SS action but somehow dies without sending a
%% 'DOWN'. This should never be able to happen, but could and there is no other
%% check for that than a (rough, very granular) comparison of timestamps. The
%% second clause implements a timecout category called "SS Message Lock Timeout"
%% as per the docs for this reason, and directly kills any timed out pending
%% client. If a very slow client were to take more time than the SMLT it may
%% still complete without any problem as long as another null request is not
%% received during that window. A properly functioning client process will
%% retire itself (thus sending its 'DOWN' message) before any of this can happen
%% in the normal case.

do_null(Pid,
        TS,
        Sysop,
        KeyName,
        State = #s{upstream = prime, sysops = Sysops, ss = none}) ->
    case maps:is_key(Sysop, Sysops) of
        true  -> do_null2(Pid, TS, Sysop, KeyName, State);
        false -> {{error, no_permission}, State}
    end;
do_null(Pid,
        TS,
        Sysop,
        KeyName,
        State = #s{upstream = prime,
                   ss       = #ss{timestamp = Then, pid = Pid, mon = Mon}}) ->
    Diff = timer:now_diff(TS, Then),
    {ok, Seconds} = zx_daemon:conf(timeout),
    Timeout = Seconds * 1000000,
    case Diff > Timeout of
        true ->
            true = demonitor(Mon, [flush]),
            true = exit(Pid, kill),
            do_null(Pid, TS, Sysop, KeyName, State#s{ss = none});
        false ->
            {{error, busy}, State}
    end;
do_null(_, _, _, _, State) ->
    {{error, not_prime}, State}.

do_null2(Pid, TS, Sysop, KeyName, State = #s{realm = Realm, keys = Keys}) ->
    case maps:find(KeyName, Keys) of
        {ok, #key{owner = Sysop}} ->
            {ok, PubKey} = zx_daemon:get_key(public, {Realm, KeyName}),
            do_null3(Pid, TS, Sysop, KeyName, PubKey, State);
        error ->
            {{error, bad_key}, State}
    end.

do_null3(Pid, TS, Sysop, KeyName, PubKey, State = #s{serial = Serial}) ->
    NextSerial = Serial + 1,
    SS = #ss{serial    = NextSerial,
             signatory = Sysop,
             keyname   = KeyName,
             timestamp = TS,
             action    = null,
             pid       = Pid,
             mon       = monitor(process, Pid)},
    Tag = {NextSerial, TS},
    {{ok, Tag, PubKey}, State#s{ss = SS}}.


-spec do_clear_ss(State) -> NewState
    when State :: state(),
         NewState :: state().

do_clear_ss(State = #s{ss = none}) ->
    State;
do_clear_ss(State = #s{ss = #ss{mon = Mon}}) ->
    true = demonitor(Mon, [flush]),
    State#s{ss = none}.


-spec do_list_users(State) -> Result
    when State   :: state(),
         Result  :: {ok, [zx:user_name()]}
                  | {error, not_prime}.

do_list_users(#s{upstream = prime, users = Users}) ->
    {ok, printable(Users)};
do_list_users(_) ->
    {error, not_prime}.


-spec do_list_packagers(Package, State) -> Result
    when Package :: zx:name(),
         State   :: state(),
         Result  :: {ok, [zx:user_name()]}
                  | {error, Reason},
         Reason  :: bad_package
                  | not_prime.

do_list_packagers(Package, #s{upstream = prime, users = Users, packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, #package{packagers = Ps}} -> {ok, printable(maps:with(Ps, Users))};
        error                          -> {error, bad_package}
    end;
do_list_packagers(_, _) ->
    {error, not_prime}.


-spec do_list_maintainers(Package, State) -> Result
    when Package :: zx:name(),
         State   :: state(),
         Result  :: {ok, [zx:user_name()]}
                  | {error, bad_package}.

do_list_maintainers(Package,
                    #s{upstream = prime, users = Users, packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, #package{maintainers = Ms}} -> {ok, printable(maps:with(Ms, Users))};
        error                            -> {error, bad_package}
    end;
do_list_maintainers(_, _) ->
    {error, not_prime}.


-spec do_list_sysops(state()) -> {ok, Result}
    when Result :: [{zx:user_name(), zx:real_name(), [zx:contact_info()]}].

do_list_sysops(#s{sysops = Sysops, users = Users}) ->
    Names = maps:keys(Sysops),
    {ok, printable(maps:with(Names, Users))}.


printable(Users) ->
    [{ID, N, C} || {ID, #user{name = N, contact = C}} <- maps:to_list(Users)].


-spec do_list_pending(Package, State) -> Result
    when Package :: zx:name(),
         State   :: state(),
         Result  :: {ok, [zx:version()]}
                  | {error, bad_package}.

do_list_pending(Package, #s{upstream = prime, packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, #package{pending = Pending}} ->
            {ok, Pending};
        error ->
            {error, bad_package}
    end;
do_list_pending(_, _) ->
    {error, not_prime}.


-spec do_list_approved(State) -> Result
    when State  :: state(),
         Result :: {ok, [{zx:name(), zx:version()}]}
                 | {error, not_prime}.

do_list_approved(#s{upstream = prime, approved = Approved}) ->
    {ok, Approved};
do_list_approved(_) ->
    {error, not_prime}.


-spec do_review(Name, Version, State) -> Result
    when Name    :: zx:name(),
         Version :: zx:version(),
         State   :: state(),
         Result  :: {ok, binary()}
                  | {error, Reason},
         Reason  :: bad_package
                  | bad_version
                  | not_prime.

do_review(Name, Version, State = #s{upstream = prime}) ->
    case do_list_pending(Name, State) of
        {ok, Pending} -> do_review2(Name, Version, Pending, State);
        Error         -> {Error, State}
    end;
do_review(_, _, State) ->
    {{error, not_prime}, State}.

do_review2(Name, Version, Pending, State) ->
    case lists:member(Version, Pending) of
        true  -> load_pending(Name, Version, State);
        false -> do_review3(Name, Version, State)
    end.

do_review3(Name, Version, State = #s{approved = Approved}) ->
    case lists:member({Name, Version}, Approved) of
        true  -> load_approved(Name, Version, State);
        false -> {{error, bad_version}, State}
    end.


load_pending(Name, Version, State = #s{realm = Realm, packages = Packages}) ->
    case load_tmp_zsp({Realm, Name, Version}) of
        {ok, Bin} ->
            {{ok, Bin}, State};
        {error, Reason} ->
            ok = tell(error, "load_tmp_zsp/1 failed with ~tw", [Reason]),
            P = #package{pending = Pending} = maps:get(Name, Packages),
            NewPending = lists:delete(Version, Pending),
            NewPackages = maps:put(Name, P#package{pending = NewPending}, Packages),
            Message = {load_fail, atom_to_list(Reason)},
            NewState = State#s{packages = NewPackages},
            ok = stash(NewState),
            {{error, Message}, NewState}
    end.


load_approved(Name, Version, State = #s{realm = Realm, approved = Approved}) ->
    case load_tmp_zsp({Realm, Name, Version}) of
        {ok, Bin} ->
            {{ok, Bin}, State};
        {error, Reason} ->
            ok = tell(error, "load_tmp_zsp/1 failed with ~tw", [Reason]),
            NewApproved = lists:delete({Name, Version}, Approved),
            Reason = {load_fail, atom_to_list(Reason)},
            NewState = State#s{approved = NewApproved},
            ok = stash(NewState),
            {{error, Reason}, NewState}
    end.


-spec do_approve(Message, State) -> {Result, NewState}
    when Message  :: su_message(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_package
                   | bad_version
                   | not_prime,
         NewState :: state().

do_approve({Sig, Signed, {Signatory, KN, {Package, Version}}}, State) ->
    case authenticate({maintainer, Package}, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_approve2(Package, Version, State);
        Error -> {Error, State}
    end.

do_approve2(Name, Version, State = #s{packages = Packages}) ->
    case maps:find(Name, Packages) of
        {ok, Package} -> mv_approved(Name, Version, Package, State);
        error         -> {{error, bad_package}, State}
    end.

mv_approved(Name,
            Version,
            P = #package{pending = Pending},
            State = #s{packages = Packages, approved = Approved}) ->
    case lists:member(Version, Pending) of
        true ->
            NewPending = lists:delete(Version, Pending),
            NewPackages = maps:put(Name, P#package{pending = NewPending}, Packages),
            NewApproved = [{Name, Version} | Approved],
            NewState = State#s{packages = NewPackages, approved = NewApproved},
            ok = stash(NewState),
            {ok, NewState};
        false ->
            {{error, bad_version}, State}
    end.


-spec do_reject(Message, State) -> {Result, NewState}
    when Message  :: su_message(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_package
                   | bad_version
                   | not_prime,
         NewState :: state().

do_reject({Sig, Signed, {Signatory, KN, {Package, Version}}}, State) ->
    case authenticate({maintainer, Package}, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_reject2(Package, Version, State);
        Error -> {Error, State}
    end.

do_reject2(Name, Version, State = #s{packages = Packages}) ->
    case maps:find(Name, Packages) of
        {ok, Package} -> do_reject3(Name, Version, Package, State);
        error         -> {{error, bad_package}, State}
    end.

do_reject3(Name,
           Version,
           P = #package{pending = Pending},
           State = #s{realm = Realm, packages = Packages, approved = Approved}) ->
    IsPending  = lists:member(Version, Pending),
    IsApproved = lists:member({Name, Version}, Approved),
    case IsPending or IsApproved of
        true ->
            NewPending = lists:delete(Version, Pending),
            NewPackages = maps:put(Name, P#package{pending = NewPending}, Packages),
            NewApproved = lists:delete({Name, Version}, Approved),
            ok = zx_lib:rm_rf(tmp_zsp_path({Realm, Name, Version})),
            NewState = State#s{packages = NewPackages, approved = NewApproved},
            ok = stash(NewState),
            {ok, NewState};
        false ->
            {{error, bad_version}, State}
    end.


-spec do_prep_accept(Package, MessageBin, AcceptMeta, State) -> {Result, NewState}
    when Package     :: {zx:name(), zx:version()},
         MessageBin  :: binary(),
         AcceptMeta  :: accept_meta(),
         State       :: state(),
         Result      :: {ok, binary()}
                      | {error, Reason},
         Reason      :: not_found
                      | bad_request
                      | unknown_user
                      | no_permission
                      | bad_key
                      | not_prime,
         NewState    :: state().

do_prep_accept(Package,
               MessageBin,
               {{Serial, TS}, Signatory, KeyName, ReqMeta},
               State = #s{upstream = prime,
                          sysops   = SysOps,
                          users    = Users,
                          keys     = Keys,
                          ss       = #ss{signatory = Signatory,
                                         serial    = Serial,
                                         keyname   = KeyName,
                                         timestamp = TS,
                                         action    = null}}) ->
    case maps:is_key(Signatory, SysOps) of
        true ->
            case maps:find(KeyName, Keys) of
                {ok, #key{owner = Signatory}} ->
                    Action = {accept, Package, MessageBin, ReqMeta},
                    do_prep_accept2(Package, Action, State);
                _ ->
                    {{error, bad_key}, State}
            end;
        false ->
            case maps:is_key(Signatory, Users) of
                true  -> {{error, no_permission}, State};
                false -> {{error, unknown_user}, State}
            end
    end;
do_prep_accept(_, _, _, State = #s{upstream = prime}) ->
    {{error, bad_request}, State};
do_prep_accept(_, _, _, State) ->
    {{error, not_prime}, State}.

do_prep_accept2(Package, Action, State = #s{approved = Approved, ss = SS}) ->
    case lists:member(Package, Approved) of
        true  -> {ok, State#s{ss = SS#ss{action = Action}}};
        false -> {{error, not_found}, State}
    end.


-spec do_accept(Pid, Tag, PackageBin, State) -> {Result, NewState}
    when Pid        :: pid(),
         Tag        :: zx:ss_tag(),
         PackageBin :: binary(),
         State      :: state(),
         Result     :: ok
                     | {error, bad_request},
         NewState   :: state().

do_accept(Pid,
          {Serial, TS},
          ZspBin,
          State = #s{serial = Current,
                     ss     = #ss{pid       = Pid,
                                  serial    = Serial,
                                  timestamp = TS}}) ->
    case Serial == Current + 1 of
        true  -> save_accepted(ZspBin, State);
        false -> {{error, bad_request}, State#s{ss = none}}
    end;
do_accept(_, _, _, State) ->
    ok = tell(info, "Received bad accept request."),
    {{error, bad_request}, State}.

save_accepted(ZspBin,
              State = #s{realm    = Realm,
                         approved = Approved,
                         enrolled = Enrolled,
                         history  = History,
                         ss       = #ss{serial    = Serial,
                                        signatory = Signatory,
                                        timestamp = Timestamp,
                                        mon       = Mon,
                                        action    = {accept,
                                                     Package = {Name, Version},
                                                     Message,
                                                     Meta}}}) ->
    PackageID = {Realm, Name, Version},
    ok = file:write_file(zx_lib:zsp_path(PackageID), ZspBin),
    NewApproved = lists:delete(Package, Approved),
    NewHistory = maps:put(Serial, Message, History),
    NextState = State#s{serial   = Serial,
                        approved = NewApproved,
                        history  = NewHistory,
                        ss       = none},
    NewState = import_accept(Package, Meta, Timestamp, Signatory, NextState),
    true = demonitor(Mon),
    ok = write_history(Realm, NewHistory),
    ok = stash(NewState),
    ok = zomp_client_man:update(Realm, Serial),
    ok = broadcast_ss(Message, sets:to_list(Enrolled)),
    {ok, NewState}.


-spec do_add_package(PackageName, State) -> Outcome
    when PackageName :: zx:package(),
         State       :: state(),
         Outcome     :: {ok, NewState}
                      | {error, exists},
         NewState    :: state().

do_add_package({Realm, Package},
               State = #s{realm    = Realm,
                          packages = Packages,
                          ss       = #ss{serial    = Serial,
                                         signatory = Signatory,
                                         timestamp = Timestamp}}) ->
    tell(info, "Adding package: ~s-~s", [Realm, Package]),
    case maps:is_key(Package, Packages) of
        false ->
            P = #package{serial    = Serial,
                         signatory = Signatory,
                         timestamp = Timestamp},
            NewPackages = maps:put(Package, P, Packages),
            NewState = State#s{serial = Serial, packages = NewPackages},
            {ok, NewState};
        true ->
            {error, exists}
    end.


-spec do_add_user(Message, State) -> {Result, NewState}
    when Message  :: su_message(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: exists
                   | {bad_key, zx:key_id()}
                   | not_prime
                   | no_permission,
         NewState :: state().

do_add_user({Sig, Signed, {Signatory, KN, UserData}}, State) ->
    case authenticate(sysop, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_add_user2(UserData, Signatory, State);
        Error -> {Error, State}
    end.

do_add_user2({UserName, RealName, ContactInfo, KeyData}, Signatory, State) ->
    Timestamp = calendar:universal_time(),
    Template = #key{signatory = Signatory, timestamp = Timestamp},
    U = #user{contact   = ContactInfo,
              name      = RealName,
              signatory = Signatory,
              timestamp = Timestamp},
    maybe_add_keys({UserName, U}, KeyData, Template, State).


-spec do_list_user_keys(UserName, State) -> Outcome
    when UserName :: zx:user_name(),
         State    :: state(),
         Outcome  :: {ok, [zx:key_name()]}
                   | {error, Reason},
         Reason   :: bad_user.

do_list_user_keys(UserName, #s{users = Users}) ->
    case maps:find(UserName, Users) of
        {ok, #user{keys = KeyNames}} -> {ok, KeyNames};
        error                        -> {error, bad_user}
    end.


-spec do_add_user_keys(Message, State) -> {Result, NewState}
    when Message  :: su_message(),
         State    :: state(),
         Result   :: ok | {error, Reason},
         Reason   :: bad_user
                   | sysop
                   | {bad_key, zx:key_name()}
                   | {bad_sig, zx:key_name()},
         NewState :: state().

do_add_user_keys({Sig, Signed, {Signatory, KN, {UserName, KeyData}}}, State) ->
    case authenticate(sysop, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_add_user_keys2(UserName, KeyData, State);
        Error -> {Error, State}
    end.

do_add_user_keys2(UserName, KeyData, State = #s{sysops = Sysops, users = Users}) ->
    IsSysop = maps:is_key(UserName, Sysops),
    Found = maps:find(UserName, Users),
    case {IsSysop, Found} of
        {false, {ok, U}} ->
            do_add_user_keys3({UserName, U}, UserName, KeyData, State);
        {true,  {ok, #user{}}} ->
            {{error, sysop}, State};
        {false, error} ->
            {{error, bad_user}, State}
    end.

do_add_user_keys3(User, Signatory, KeyData, State) ->
    Template = #key{signatory = Signatory, timestamp = calendar:universal_time()},
    maybe_add_keys(User, KeyData, Template, State).


maybe_add_keys({UserName, U = #user{keys = CurrentKeys}},
               KeyData,
               Template,
               State = #s{realm = Realm, users = Users, keys = Keys}) ->
    Temp = Template#key{owner = UserName},
    case maybe_add_keys2(KeyData, Realm, CurrentKeys, Temp, #{}) of
        {ok, Added} ->
            NewKeyNames = lists:append(maps:keys(Added), CurrentKeys),
            NewU = U#user{keys = NewKeyNames},
            NewKeys = maps:merge(Added, Keys),
            NewUsers = maps:put(UserName, NewU, Users),
            NewState = State#s{users = NewUsers, keys = NewKeys},
            Register = fun(K) -> zx_daemon:register_key(Realm, K) end,
            ok = lists:foreach(Register, KeyData),
            ok = stash(NewState),
            {ok, NewState};
        Error ->
            {Error, State}
    end.

maybe_add_keys2([{KeyName, {Sig, Bin}, none} | Rest],
                Realm,
                KeyNames,
                Template,
                Acc) ->
    KeyID = {Realm, KeyName},
    Registered = lists:member(KeyName, KeyNames),
    AlreadyPresent = zx_daemon:have_key(public, KeyID),
    case Registered or AlreadyPresent of
        false ->
            case verify_key(Realm, Sig, Bin) of
                true ->
                    KeyRecord = Template#key{chain_sig = Sig},
                    NewAcc = maps:put(KeyName, KeyRecord, Acc),
                    maybe_add_keys2(Rest, Realm, KeyNames, Template, NewAcc);
                false ->
                    {error, {bad_key, KeyID}};
                Error ->
                    Error
            end;
        true ->
            maybe_add_keys2(Rest, Realm, KeyNames, Template, Acc)
    end;
maybe_add_keys2([{KeyName, _, {_, _}} | _], Realm, _, _, _) ->
    {error, {bad_key, {Realm, KeyName}}};
maybe_add_keys2([], _, _, _, Acc) ->
    {ok, Acc}.

verify_key(Realm, {SigKeyName, Sig}, KeyBin) ->
    case zx_daemon:get_key(public, {Realm, SigKeyName}) of
        {ok, PubKey}    -> zx_key:verify(KeyBin, Sig, PubKey);
        {error, Reason} -> {error, {Reason, {Realm, SigKeyName}}}
    end.


-spec do_add_packager(Data, State) -> {Result, NewState}
    when Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package  :: zx:name(),
                        Packager :: zx:user_name()}}},
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package,
         NewState :: state().

do_add_packager({Sig, Signed, {Signatory, KN, {Package, Packager}}}, State) ->
    case authenticate({maintainer, Package}, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_add_packager2(Package, Packager, State);
        Error -> {Error, State}
    end.

do_add_packager2(Package, Packager, State = #s{users = Users}) ->
    case maps:find(Packager, Users) of
        {ok, U} -> do_add_packager3(Package, Packager, U, State);
        error   -> {{error, bad_user}, State}
    end.

do_add_packager3(Package, Packager, U, State = #s{packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, P} -> do_add_packager4(Package, Packager, U, P, State);
        error   -> {{error, bad_package}, State}
    end.

do_add_packager4(Package,
                 Packager,
                 U = #user{packaging = Packaging},
                 P = #package{packagers = Packagers},
                 State = #s{users = Users, packages = Packages}) ->
    case lists:member(Package, Packaging) of
        false ->
            NewU = U#user{packaging = [Package | Packaging]},
            NewP = P#package{packagers = [Packager | Packagers]},
            NewUsers = maps:put(Packager, NewU, Users),
            NewPackages = maps:put(Package, NewP, Packages),
            NewState = State#s{users = NewUsers, packages = NewPackages},
            ok = stash(NewState),
            {ok, NewState};
        true ->
            {ok, State}
    end.


-spec do_rem_packager(Data, State) -> {Result, NewState}
    when Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package  :: zx:name(),
                        Packager :: zx:user_name()}}},
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package,
         NewState :: state().

do_rem_packager({Sig, Signed, {Signatory, KN, {Package, Packager}}}, State) ->
    case authenticate({maintainer, Package}, {Sig, Signed, Signatory, KN}, State) of
        ok    -> do_rem_packager2(Package, Packager, State);
        Error -> {Error, State}
    end.

do_rem_packager2(Package, UserName, State = #s{users = Users}) ->
    case maps:find(UserName, Users) of
        {ok, U} -> do_rem_packager3(Package, UserName, U, State);
        error   -> {{error, bad_user}, State}
    end.

do_rem_packager3(Package, UserName, U, State = #s{packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, P} -> do_rem_packager4(Package, UserName, U, P, State);
        error   -> {{error, bad_package}, State}
    end.

do_rem_packager4(Package,
                 UserName,
                 U = #user{packaging = Packaging},
                 P = #package{packagers = Packagers},
                 State = #s{users = Users, packages = Packages}) ->
    NewU = U#user{packaging = lists:delete(Package, Packaging)},
    NewP = P#package{packagers = lists:delete(UserName, Packagers)},
    NewUsers = maps:put(UserName, NewU, Users),
    NewPackages = maps:put(Package, NewP, Packages),
    NewState = State#s{users = NewUsers, packages = NewPackages},
    ok = stash(NewState),
    {ok, NewState}.


-spec do_add_maintainer(Data, State) -> {Result, NewState}
    when Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package    :: zx:name(),
                        Maintainer :: zx:user_name()}}},
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package,
         NewState :: state().

do_add_maintainer({Sig, Signed, {Signatory, KeyName, {Package, Packager}}}, State) ->
    case authenticate(sysop, {Sig, Signed, Signatory, KeyName}, State) of
        ok    -> do_add_maintainer2(Package, Packager, State);
        Error -> {Error, State}
    end.

do_add_maintainer2(Package, UserName, State = #s{users = Users}) ->
    case maps:find(UserName, Users) of
        {ok, U} -> do_add_maintainer3(Package, UserName, U, State);
        error   -> {{error, bad_user}, State}
    end.

do_add_maintainer3(Package, UserName, U, State = #s{packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, P} -> do_add_maintainer4(Package, UserName, U, P, State);
        error   -> {{error, bad_package}, State}
    end.

do_add_maintainer4(Package,
                   UserName,
                   U = #user{maintaining = Maintaining},
                   P = #package{maintainers = Maintainers},
                   State = #s{users = Users, packages = Packages}) ->
    case lists:member(Package, Maintaining) of
        false ->
            NewU = U#user{maintaining = [Package | Maintaining]},
            NewP = P#package{maintainers = [UserName | Maintainers]},
            NewUsers = maps:put(UserName, NewU, Users),
            NewPackages = maps:put(Package, NewP, Packages),
            NewState = State#s{users = NewUsers, packages = NewPackages},
            ok = stash(NewState),
            {ok, NewState};
        true ->
            {ok, State}
    end.


-spec do_rem_maintainer(Data, State) -> {Result, NewState}
    when Data     :: {Sig    :: binary(),
                      Signed :: binary(),
                      {Signatory :: zx:user_name(),
                       KeyName   :: zx:key_name(),
                       {Package    :: zx:name(),
                        Maintainer :: zx:user_name()}}},
         State    :: state(),
         Result   :: ok
                   | {error, Reason},
         Reason   :: bad_user
                   | bad_package,
         NewState :: state().

do_rem_maintainer({Sig, Signed, {Signatory, KeyName, {Package, Maintainer}}}, State) ->
    case authenticate(sysop, {Sig, Signed, Signatory, KeyName}, State) of
        ok    -> do_rem_maintainer2(Package, Maintainer, State);
        Error -> {Error, State}
    end.

do_rem_maintainer2(Package, UserName, State = #s{users = Users}) ->
    case maps:find(UserName, Users) of
        {ok, U} -> do_rem_maintainer3(Package, UserName, U, State);
        error   -> {{error, bad_user}, State}
    end.

do_rem_maintainer3(Package, UserName, U, State = #s{packages = Packages}) ->
    case maps:find(Package, Packages) of
        {ok, P} -> do_rem_maintainer4(Package, UserName, U, P, State);
        error   -> {{error, bad_package}, State}
    end.

do_rem_maintainer4(Package,
                   UserName,
                   U = #user{maintaining = Maintaining},
                   P = #package{maintainers = Maintainers},
                   State = #s{users = Users, packages = Packages}) ->
    NewU = U#user{maintaining = lists:delete(Package, Maintaining)},
    NewP = P#package{maintainers = lists:delete(UserName, Maintainers)},
    NewUsers = maps:put(UserName, NewU, Users),
    NewPackages = maps:put(Package, NewP, Packages),
    NewState = State#s{users = NewUsers, packages = NewPackages},
    ok = stash(NewState),
    {ok, NewState}.


-spec do_fetch(Package, State) -> Result
    when Package :: {zx:name(), zx:version()},
         State   :: state(),
         Result  :: {ok, binary()}
                  | upstream
                  | {error, bad_package | bad_version}.

do_fetch(Package = {Name, Version},
         #s{realm = Realm, packages = Packages, versions = Versions}) ->
    case maps:is_key(Package, Versions) of
        true ->
            Path = zx_lib:zsp_path({Realm, Name, Version}),
            case file:read_file(Path) of
                {ok, Bin}       -> {ok, Bin};
                {error, enoent} -> upstream
            end;
        false ->
            case maps:is_key(Name, Packages) of
                true  -> {error, bad_version};
                false -> {error, bad_package}
            end
    end.


-spec do_cache(Package, Bin, State) -> Result
    when Package :: {zx:name(), zx:version()},
         Bin     :: binary(),
         State   :: state(),
         Result  :: ok
                  | {error, bad_package | bad_sig}.

do_cache(Package = {Name, Version}, Bin, #s{realm = Realm, versions = Versions}) ->
    case maps:is_key(Package, Versions) of
        true  -> do_cache2({Realm, Name, Version}, Bin);
        false -> {error, bad_package}
    end.

do_cache2(PackageID, Bin) ->
    case zx_zsp:package_id(Bin) of
        {ok, PackageID} ->
            do_cache3(PackageID, Bin);
        {ok, Other} ->
            Message = "PackageID requested: ~p, PackageID of zsp: ~p",
            ok = tell(error, Message, [PackageID, Other]),
            {error, bad_package};
        Error ->
            Error
    end.

do_cache3(PackageID, Bin) ->
    case zx_zsp:verify(Bin) of
        ok    -> file:write_file(zx_lib:zsp_path(PackageID), Bin);
        Error -> Error
    end.


-spec do_keychain(KeyName, State) -> Result
    when KeyName  :: zx:key_name(),
         State    :: state(),
         Result   :: {ok, [{zx:user_name(), zx:key_data()}]}
                   | {error, Reason},
         Reason   :: {bad_key, zx:key_name()}
                    | no_pub
                    | bad_key.

do_keychain(KeyName, #s{realm = Realm, keys = Keys}) ->
    do_keychain2(Realm, KeyName, Keys, []).

do_keychain2(Realm, KeyName, Keys, Acc) ->
    case maps:find(KeyName, Keys) of
        {ok, #key{chain_sig = root}} ->
            Acc;
        {ok, #key{owner = Owner, chain_sig = Sig}} ->
            case zx_daemon:get_key(public, {Realm, KeyName}) of
                {ok, KeyBin} ->
                    NewAcc = [{Owner, {KeyName, {Sig, KeyBin}, none}} | Acc],
                    NextKeyName = element(1, Sig),
                    do_keychain2(Realm, NextKeyName, Keys, NewAcc);
                Error ->
                    Error
            end;
        error ->
            {error, {bad_key, KeyName}}
    end.


-spec do_prep_submit(Submitter, KeyName, Package, State) -> {Result, NewState}
    when Submitter :: zx:user_name(),
         KeyName   :: zx:key_name(),
         Package   :: zx:name(),
         State     :: state(),
         Result    :: {ok, PublicKey :: term()}
                    | {error, Reason},
         NewState  :: state(),
         Reason    :: bad_key
                    | bad_user
                    | bad_package
                    | unknown_user
                    | no_permission.

do_prep_submit(Submitter,
               KeyName,
               Package,
               State = #s{realm = Realm, users = Users, keys = Keys}) ->
    case maps:find(KeyName, Keys) of
        {ok, #key{owner = Submitter}} ->
            {ok, PubKey} = zx_daemon:get_key(public, {Realm, KeyName}),
            do_prep_submit2(Submitter, PubKey, Package, State);
        {ok, #key{}} ->
            case maps:is_key(Submitter, Users) of
                true  -> {{error, bad_user}, State};
                false -> {{error, unknown_user}, State}
            end;
        error ->
            {{error, bad_key}, State}
    end.

do_prep_submit2(Submitter,
                PubKey,
                Package,
                State = #s{sysops = Sysops, packages = Packages}) ->
    Result =
        case maps:find(Package, Packages) of
            {ok, #package{packagers = Packagers, maintainers = Maintainers}} ->
                HasAuth =  maps:is_key(Submitter, Sysops)
                    orelse lists:member(Submitter, Packagers)
                    orelse lists:member(Submitter, Maintainers),
                case HasAuth of
                    true  -> {ok, PubKey};
                    false -> {error, no_permission}
                end;
            error ->
                {error, bad_package}
        end,
    {Result, State}.


-spec do_submit(Name, Version, Bin, State) -> {Result, NewState}
    when Name     :: zx:name(),
         Version  :: zx:version(),
         Bin      :: binary(),
         State    :: state(),
         Result   :: ok | {error, Reason},
         Reason   :: bad_package
                   | bad_version
                   | not_prime,
         NewState :: state().

do_submit(Name,
          Version,
          Bin,
          State = #s{upstream = prime, packages = Packages}) ->
    case maps:find(Name, Packages) of
        {ok, P} -> do_submit(Name, Version, Bin, P, State);
        error   -> {{error, bad_package}, State}
    end;
do_submit(_, _, _, State) ->
    {{error, not_prime}, State}.

do_submit(Name,
          Version,
          Bin,
          P = #package{versions = Versions, pending = Pending},
          State = #s{realm = Realm, approved = Approved, packages = Packages}) ->
    Invalid =  lists:member(Version, Versions)
        orelse lists:member(Version, Pending)
        orelse lists:member({Name, Version}, Approved),
    case Invalid of
        false ->
            ok = save_tmp_zsp({Realm, Name, Version}, Bin),
            NewPending = [Version | Pending],
            NewPackages = maps:put(Name, P#package{pending = NewPending}, Packages),
            NewState = State#s{packages = NewPackages},
            ok = stash(NewState),
            {ok, NewState};
        true ->
            {{error, bad_version}, State}
    end.


-spec sync_history(Serial, State) -> ok
    when Serial :: zx:serial(),
         State  :: state().

sync_history(Latest, #s{realm = Realm, upstream = US, serial = Current})
        when Latest > Current ->
    Node = element(1, US),
    Next = Current + 1,
    case Latest == Next of
        true  -> zomp_node:pull_event(Node, Realm, Latest);
        false -> zomp_node:pull_span(Node, Realm, {Next, Latest})
    end;
sync_history(Latest, #s{serial = Current}) when Latest =< Current ->
    ok.


-spec do_upstream(Conn, State) -> NewState
    when Conn     :: pid(),
         State    :: state(),
         NewState :: state().

do_upstream(Conn, State = #s{upstream = prime}) ->
    Message = "Recieved illegal upstream reset to ~tp while acting as prime",
    ok = tell(error, Message, [Conn]),
    State;
do_upstream(Conn, State = #s{upstream = none}) ->
    Mon = monitor(process, Conn),
    State#s{upstream = {Conn, Mon}};
do_upstream(Conn, State = #s{upstream = {Conn, _}}) ->
    State;
do_upstream(Conn, State = #s{upstream = {_, OldMon}}) ->
    true = demonitor(OldMon),
    Mon = monitor(process, Conn),
    State#s{upstream = {Conn, Mon}}.


-spec do_update(Message, State) -> NewState
    when Message  :: binary(),
         State    :: state(),
         NewState :: state().

do_update(Message, State = #s{upstream = none}) ->
    M = "Received an update/2 instruction without any upstream. Update message: ~250tp",
    ok = tell(error, M, [Message]),
    State;
do_update(Message, State = #s{upstream = prime}) ->
    M = "Received an update/2 instruction while prime. Update message: ~250tp",
    ok = tell(error, M, [Message]),
    State;
do_update(Message, State = #s{upstream = {Pid, Mon}, serial = Serial}) ->
    case interpret_event(Message, State#s{serial = Serial + 1}) of
        {ok, NewState} ->
            NewState;
        error ->
            true = demonitor(Mon, [flush]),
            ok = zomp_node:stop(Pid),
            M =
                "Received bad update/2 instruction from upstream. "
                "Update message: ~250tp",
            ok = tell(error, M, [Message]),
            State#s{upstream = none}
    end.


-spec handle_down(Mon, Pid, Info, State) -> NewState
    when Mon      :: reference(),
         Pid      :: pid(),
         Info     :: term(),
         State    :: state(),
         NewState :: state().

handle_down(Mon, Pid, Info, State = #s{upstream = {Pid, Mon}}) ->
    ok = tell("Upstream connection retired with ~200tp", [Info]),
    State#s{upstream = none};
handle_down(Mon, Pid, Info, State = #s{ss = #ss{pid = Pid, mon = Mon}}) ->
    ok = tell(warning, "SS AUTH requester died incomplete with ~200tp", [Info]),
    State#s{ss = none};
handle_down(Mon,
            Pid,
            Info,
            State = #s{enrolled = Enrolled, subs = Subs, mons = Mons}) ->
    case maps:take(Pid, Mons) of
        {{Mon, true, Subscriptions}, NewMons} ->
            ok = tell(warning, "NODE client ~tp died with ~200tp", [Pid, Info]),
            NewEnrolled = sets:del_element(Pid, Enrolled),
            NewSubs = scrub_subs(Pid, Subscriptions, Subs),
            State#s{enrolled = NewEnrolled, subs = NewSubs, mons = NewMons};
        {{Mon, false, Subscriptions}, NewMons} ->
            ok = tell(warning, "LEAF client died with ~200tp", [Info]),
            NewSubs = scrub_subs(Pid, Subscriptions, Subs),
            State#s{subs = NewSubs, mons = NewMons};
        error ->
            Unexpected = {'DOWN', Mon, process, Pid, Info},
            ok = tell(warning, "Unexpected info: ~200tp", [Unexpected]),
            State
    end.

scrub_subs(Pid, Subscriptions, Subs) ->
    Del = fun(Pids) -> sets:del_element(Pid, Pids) end,
    Scrub = fun(S, Ss) -> maps:update_with(S, Del, Ss) end,
    lists:foldl(Scrub, Subs, sets:to_list(Subscriptions)).



%%% State Persistence and Recovery

ensure_dirs(Realm) ->
    Dirs = [etc, var, tmp, log, key, zsp, lib],
    Force = fun(D) -> zx_lib:force_dir(zx_lib:path(D, Realm)) end,
    lists:foreach(Force, Dirs).


stash(State = #s{realm = Realm, modules = Modules}) ->
    Data = State#s{modules = maps:map(fun(_, V) -> sets:to_list(V) end, Modules)},
    Path = stash_path(Realm),
    ok = filelib:ensure_dir(Path),
    Bin = term_to_binary(Data),
    file:write_file(Path, Bin).


write_history(Realm, Log) ->
    Bin = term_to_binary(Log),
    Path = history_path(Realm),
    file:write_file(Path, Bin).


maybe_load_stash(State = #s{realm = Realm}) ->
    {ok, NextState} = read_history(State),
    case file:read_file(stash_path(Realm)) of
        {ok, Bin} ->
            ok = tell(info, "Loaded realm state from stash file."),
            load_stash(NextState, Bin);
        {error, enoent} ->
            ok = tell(info, "Stash file not found. Importing history."),
            #s{history = Log} = NextState,
            {ok, NewState} = bootstrap(NextState),
            import_history(NewState#s{history = Log})
    end.


load_stash(State = #s{realm = Realm}, Bin) ->
    case zx_lib:b_to_t(Bin) of
        {ok, #s{serial = 0}} ->
            reboot_history(State);
        {ok, Stash = #s{modules = Modules}} ->
            ModSet = fun(_, V) -> sets:from_list(V) end,
            NewState = Stash#s{modules = maps:map(ModSet, Modules)},
            {ok, NewState};
        error ->
            Message = "Failed to read stash for ~tw. Importing history",
            ok = tell(warning, Message, [Realm]),
            import_history(State)
    end.


read_history(State = #s{realm = Realm}) ->
    History = history_path(Realm),
    case file:read_file(History) of
        {ok, Bin} ->
            read_history(Bin, State);
        {error, enoent} ->
            Message = "History file ~ts not found. Starting blank.",
            ok = tell(info, Message, [History]),
            reboot_history(State)
    end.


read_history(Bin, State) ->
    case zx_lib:b_to_ts(Bin) of
        {ok, Log} ->
            Size = maps:size(Log),
            ok = tell(info, "History log read with ~p entries.", [Size]),
            {ok, State#s{serial = 1, history = Log}};
        error ->
            reboot_history(State)
    end.


import_history(State = #s{history = Log}) ->
    Size = maps:size(Log),
    ok = tell(info, "Importing, ~p history events...", [Size]),
    import_history(State#s{serial = 1}, Size).


import_history(State = #s{serial = Serial}, Last) when Serial > Last ->
    {ok, State#s{serial = Last}};
import_history(State = #s{serial = Serial, history = Log}, Last) ->
    case maps:find(Serial, Log) of
        {ok, Message} ->
            case interpret_event(Message, State) of
                {ok, NewState} -> import_history(NewState#s{serial = Serial + 1}, Last);
                error          -> reboot_history(State)
            end;
        error ->
            reboot_history(State)
    end.


interpret_event(Bin = <<Size:24, Sig:Size/binary, Signed/binary>>,
                State = #s{serial = Serial}) ->
    case decode(Signed) of
        {ok, {Command, {{Serial, TS}, Signatory, KeyName, Data}}} ->
            Meta = {Command, TS, Signatory, Data},
            verify_event(Signatory, Signed, Sig, KeyName, Bin, Meta, State);
        {ok, Wrong} ->
            Message = "Out of sequence event at serial ~w. Rebooting history.",
            ok = tell(error, Message, [Serial]),
            ok = tell(error, "Bad import data: ~250tp", [Wrong]),
            error;
        {error, bad_message} ->
            ok = tell(error, "Bad SS event #~w. Rebooting history.", [Serial]),
            error
    end.

verify_event(Signatory, Signed, Sig, KeyName, Bin, Meta, State = #s{sysops = Sysops}) ->
    case maps:is_key(Signatory, Sysops) of
        true ->
            verify_event2(Signatory, Signed, Sig, KeyName, Bin, Meta, State);
        false ->
            ok = tell(warning, "Invalid signatory: ~250tp", [Signatory]),
            error
    end.

verify_event2(Signatory, Signed, Sig, KeyName, Bin, Meta, State = #s{keys = Keys}) ->
    case maps:find(KeyName, Keys) of
        {ok, #key{owner = Signatory}} ->
            verify_event3(Signed, Sig, KeyName, Bin, Meta, State);
        error ->
            error
    end.

verify_event3(Signed, Sig, KeyName, Bin, Meta, State = #s{realm = Realm}) ->
    KeyID = {Realm, KeyName},
    case zx_daemon:get_key(public, KeyID) of
        {ok, Key} ->
            verify_event4(Signed, Sig, Key, Bin, Meta, State);
        {error, Reason} ->
            Message = "zx_daemon:get_key(private, ~250tp) failed with: ~tp",
            ok = tell(warning, Message, [KeyID, Reason]),
            error
    end.

verify_event4(Signed,
              Sig,
              Key,
              Message,
              Meta,
              State = #s{realm    = Realm,
                         serial   = Serial,
                         history  = History,
                         enrolled = Enrolled}) ->
    case zx_key:verify(Signed, Sig, Key) of
        true ->
            NewHistory = maps:put(Serial, Message, History),
            NewState = import_event(Meta, State#s{history = NewHistory}),
            ok = write_history(Realm, NewHistory),
            ok = stash(NewState),
            ok = zomp_client_man:update(Realm, Serial),
            ok = broadcast_ss(Message, sets:to_list(Enrolled)),
            {ok, NewState};
        false ->
            ok = tell(warning, "Could not verify event. Rebooting history."),
            error
    end.

import_event({add_package, Timestamp, Signatory, {Realm, Name}},
             State = #s{realm = Realm}) ->
    import_add_package(Name, Timestamp, Signatory, State);
import_event({accept, Timestamp, Signatory, {Package, Meta, Size}}, State) ->
    import_accept(Package, {Meta, Size}, Timestamp, Signatory, State).


import_add_package(Name,
                   Timestamp,
                   Signatory,
                   State = #s{serial = Serial, packages = Packages}) ->
    P = #package{serial    = Serial,
                 signatory = Signatory,
                 timestamp = Timestamp},
    NewPackages = maps:put(Name, P, Packages),
    State#s{packages = NewPackages}.


import_accept(Package = {Name, Version},
              {Meta, Size},
              Timestamp,
              Signatory,
              State = #s{serial   = Serial,
                         packages = Packages,
                         versions = Versions,
                         modules  = Modules,
                         tags     = Tags}) ->
    P = #package{versions = Vs} = maps:get(Name, Packages),
    NewP = P#package{versions = [Version | Vs]},
    V = #version{meta      = Meta,
                 size      = Size,
                 serial    = Serial,
                 signatory = Signatory,
                 timestamp = Timestamp},
    Deps = maps:get(deps, Meta),
    Mods = maps:get(modules, Meta),
    SearchTags = maps:get(tags, Meta),
    NextState = State#s{packages = maps:put(Name, NewP, Packages),
                        versions = maps:put(Package, V, Versions),
                        modules  = update_mods(Package, Mods, Modules),
                        tags     = update_tags(Package, SearchTags, Tags)},
    NewState = update_deps(Deps, Package, NextState),
    update_types(Meta, NewState).

update_mods(Package, Mods, Modules) ->
    Add = fun(Packages) -> sets:add_element(Package, Packages) end,
    Init = sets:from_list([Package]),
    Upsert = fun(M, Ms) -> maps:update_with(M, Add, Init, Ms) end,
    lists:foldl(Upsert, Modules, Mods).

update_tags({Name, Version}, [Tag | Rest], Tags) ->
    NextTags =
        case maps:find(Tag, Tags) of
            {ok, Related} ->
                NewRelated =
                    case maps:find(Name, Related) of
                        {ok, Vs} -> maps:put(Name, [Version | Vs], Related);
                        error    -> maps:put(Name, [Version], Related)
                    end,
                maps:put(Tag, NewRelated, Tags);
            error ->
                maps:put(Tag, #{Name => [Version]}, Tags)
        end,
    update_tags({Name, Version}, Rest, NextTags);
update_tags(_, [], Tags) ->
    Tags.

update_deps([Dep = {Realm, Name, Version} | Rest],
            Package = {N, V},
            State = #s{realm = Realm, versions = Versions}) ->
    Dependent = {Realm, N, V},
    {ok, DependentS} = zx_lib:package_string(Dependent),
    {ok, DependsS} = zx_lib:package_string(Dep),
    DepP = {Name, Version},
    case maps:find(DepP, Versions) of
        {ok, Ver = #version{dependents = Dependents}} ->
            ok = log(info, "Adding ~s as dependent of ~s", [DependentS, DependsS]),
            NewV = Ver#version{dependents = [Dependent | Dependents]},
            NewVersions = maps:put(DepP, NewV, Versions),
            update_deps(Rest, Package, State#s{versions = NewVersions});
        error ->
            ok = log(warning, "Could not add non-existant dependency ~s!", [DependsS]),
            update_deps(Rest, Package, State)
    end;
update_deps([], _, State) ->
    State;
update_deps([Dep | Rest], Package, State) ->
    {ok, DepString} = zx_lib:package_string(Dep),
    ok = log(info, "Can't track cross-realm dep: ~s", [DepString]),
    update_deps(Rest, Package, State).

update_types(#{package_id := PackageID = {Realm, Name, Version}, type := Type},
             State = #s{types = Types}) ->
    {ok, Latest} = do_latest(Name, State),
    case Version == Latest of
        true ->
            Scrubbed = scrub_types({Realm, Name}, Types),
            NewTypes = add_type(Type, PackageID, Scrubbed),
            State#s{types = NewTypes};
        false ->
            tell(info, "Version: ~p, Latest: ~p", [Version, Latest]),
            State
    end.

scrub_types(Package, #types{gui = GUIs, cli = CLIs, app = Apps, lib = Libs}) ->
    Scrub = fun(ID) -> erlang:delete_element(3, ID) =/= Package end,
    #types{gui = lists:filter(Scrub, GUIs),
           cli = lists:filter(Scrub, CLIs),
           app = lists:filter(Scrub, Apps),
           lib = lists:filter(Scrub, Libs)}.

add_type(gui, PackageID, T = #types{gui = GUIs}) ->
    T#types{gui = [PackageID | GUIs]};
add_type(cli, PackageID, T = #types{cli = CLIs}) ->
    T#types{cli = [PackageID | CLIs]};
add_type(app, PackageID, T = #types{app = Apps}) ->
    T#types{app = [PackageID | Apps]};
add_type(lib, PackageID, T = #types{lib = Libs}) ->
    T#types{lib = [PackageID | Libs]}.


reboot_history(#s{realm = Realm, upstream = Upstream}) ->
    ok = tell(info, "Rebooting realm history for realm ~ts...", [Realm]),
    ok = zx_lib:rm_rf(history_path(Realm)),
    ok = refresh_dirs([zsp, lib, tmp], Realm),
    bootstrap(#s{realm = Realm, upstream = Upstream}).


refresh_dirs(Types, Realm) ->
    Refresh =
        fun(Type) ->
            Path = zx_lib:path(Type, Realm),
            ok = zx_lib:rm_rf(Path),
            case filelib:is_file(Path) of
                true  -> ok;
                false -> zx_lib:force_dir(Path)
            end
        end,
    lists:foreach(Refresh, Types).


bootstrap(State = #s{realm = Realm}) ->
    {ok, RealmConf} = zx_lib:load_realm_conf(Realm),
    RootKey = maps:get(key, RealmConf),
    KeyID = {Realm, RootKey},
    case zx_daemon:get_key(public, KeyID) of
        {error, _} ->
            case file:read_file(zx_key:path(public, KeyID)) of
                {ok, PubBin} ->
                    UserName = maps:get(sysop, RealmConf),
                    Owner = {Realm, UserName},
                    KeyData = {RootKey, {none, PubBin}, none},
                    ok = zx_daemon:register_key(Owner, KeyData),
                    bootstrap2(RealmConf, RootKey, State);
                {error, enoent} ->
                    Message =
                        "The realm's root key is missing. You will need to reinstall "
                        "the realmfile.~n"
                        "Do `zx drop realm ~ts` and then `zx import realm ~ts.zrf`",
                    ok = tell(error, Message, [Realm, Realm]),
                    {stop, "No realm root key."}
            end;
        {ok, _} ->
            bootstrap2(RealmConf, RootKey, State)
    end.

bootstrap2(RealmConf, RootKey, State = #s{realm = Realm}) ->
    Timestamp = maps:get(timestamp, RealmConf),
    {UserName, RealName, ContactInfo} = maps:get(sysop, RealmConf),
    URecord = #user{keys      = [RootKey],
                    contact   = ContactInfo,
                    name      = RealName,
                    signatory = UserName,
                    timestamp = Timestamp},
    Users = #{UserName => URecord},
    SRecord = #sysop{serial    = 0,
                     signatory = UserName,
                     timestamp = Timestamp},
    Sysops = #{UserName => SRecord},
    KRecord = #key{owner     = UserName,
                   chain_sig = root,
                   signatory = UserName,
                   timestamp = Timestamp},
    Keys = #{RootKey => KRecord},
    NewState = State#s{sysops = Sysops, users = Users, keys = Keys},
    ok = stash(NewState),
    ok = tell("Bootstrapped realm ~ts with sysop ~ts", [Realm, UserName]),
    {ok, NewState}.



%%% Helper Functions

-spec authenticate(Type, SigData, State) -> ok | {error, Reason}
    when Type     :: sysop
                   | {maintainer, zx:name()}
                   | {packager, zx:name()},
         SigData  :: {Sig       :: binary(),
                      Signed    :: binary(),
                      Signatory :: zx:user_name(),
                      KeyName   :: zx:key_name()},
         State    :: state(),
         Reason   :: bad_user
                   | bad_auth
                   | bad_key
                   | bad_sig.

authenticate(sysop, SigData, State = #s{upstream = prime, sysops = Sysops}) ->
    Signatory = element(3, SigData),
    case maps:is_key(Signatory, Sysops) of
        true  -> verify(SigData, State);
        false -> {error, bad_user}
    end;
authenticate({maintainer, Package},
             SigData,
             State = #s{upstream = prime, sysops = Sysops, users = Users}) ->
    Signatory = element(3, SigData),
    case maps:find(Signatory, Users) of
        {ok, #user{maintaining = Ms}} ->
            HasAuth = maps:is_key(Signatory, Sysops) orelse lists:member(Package, Ms),
            case HasAuth of
                true  -> verify(SigData, State);
                false -> {error, bad_auth}
            end;
        error ->
            {error, bad_user}
    end;
authenticate({packager, Package},
             SigData,
             State = #s{upstream = prime, sysops = Sysops, users = Users}) ->
    Signatory = element(3, SigData),
    case maps:find(Signatory, Users) of
        {ok, #user{maintaining = Ms, packaging = Ps}} ->
            HasAuth =  maps:is_key(Signatory, Sysops)
                orelse lists:member(Package, Ms)
                orelse lists:member(Package, Ps),
            case HasAuth of
                true  -> verify(SigData, State);
                false -> {error, bad_auth}
            end;
        error ->
            {error, bad_user}
    end;
authenticate(_, _, _) ->
    {error, not_prime}.


verify({Sig, Signed, Signatory, KeyName},
       #s{realm = Realm, keys = Keys}) ->
    case maps:find(KeyName, Keys) of
        {ok, #key{owner = Signatory}} ->
            {ok, PubKey} = zx_daemon:get_key(public, {Realm, KeyName}),
            verify2(Signed, Sig, PubKey);
        error ->
            {error, bad_key}
    end.

verify2(Signed, Sig, PubKey) ->
    case zx_key:verify(Signed, Sig, PubKey) of
        true  -> ok;
        false -> {error, bad_sig}
    end.


save_tmp_zsp(PackageID, Bin) ->
    Path = tmp_zsp_path(PackageID),
    ok = filelib:ensure_dir(Path),
    file:write_file(Path, Bin).


load_tmp_zsp(PackageID) ->
    file:read_file(tmp_zsp_path(PackageID)).


tmp_zsp_path(PackageID) ->
    filename:join(zx_lib:path(tmp, "otpr", "zomp"), zx_lib:zsp_name(PackageID)).


-spec broadcast_ss(Message, Enrolled) -> ok
    when Message  :: binary(),
         Enrolled :: [pid()].

broadcast_ss(_, []) ->
    ok;
broadcast_ss(Message, Enrolled) ->
    ok = tell("Broadcasting to enrolled: ~tp", [Enrolled]),
    Notify = fun(Node) -> zomp_client:forward(Node, Message) end,
    lists:foreach(Notify, Enrolled).


-spec decode(Message :: binary()) -> Result
    when Result  :: {Command, Payload}
                  | {error, bad_message},
         Command :: accept
                  | add_package,
         Payload :: {Serial    :: zx:serial(),
                     Timestamp :: erlang:timestamp(),
                     Signatory :: zx:user_name(),
                     KeyName   :: zx:key_name(),
                     Target    :: tuple()}.

decode(<<Code:8, Bin/binary>>) ->
    case zx_lib:b_to_ts(Bin) of
        {ok, Payload} -> decode(Code, Payload);
        error         -> {error, bad_message}
    end.


decode(14, Payload) -> {ok, {accept,        Payload}};
decode(15, Payload) -> {ok, {add_package,   Payload}};
decode(_, _)        -> {error, bad_message}.


stash_path(Realm) ->
    filename:join(zx_lib:path(var, Realm), "realm.stash").


history_path(Realm) ->
    filename:join(zx_lib:path(var, Realm), "history.log").


types() ->
    #types{}.
