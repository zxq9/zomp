%%% @doc
%%% The Zomp Node Supervisor
%%%
%%% This supervisor maintains the lifecycle of all zomp_node worker processes.
%%% @end

-module(zomp_node_sup).
-vsn("0.6.3").
-behavior(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start_node/3]).
-export([start_link/0]).
-export([init/1]).


%%% Interface Functions

-spec start_node(Target, Desired, ExternalPort) -> Result
    when Target       :: zx:host(),
         Desired      :: [zx:realm()],
         ExternalPort :: inet:port_number(),
         Result       :: {ok, pid()}
                       | {error, term()}.
%% @doc
%% Start a node connection handler.
%% (Should only be called from zomp_node).

start_node(Target, Desired, ExternalPort) ->
    supervisor:start_child(?MODULE, [Target, Desired, ExternalPort]).



%%% Startup

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by zomp:start/0.
%%
%% Spawns a single, registered worker process.
%%
%% Error conditions, supervision strategies, and other important issues are
%% explained in the supervisor module docs:
%% http://erlang.org/doc/man/supervisor.html

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% Do not call this function directly -- it is exported only because it is a
%% necessary part of the OTP supervisor behavior.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},

    Node = {zomp_node,
            {zomp_node, start_link, []},
            temporary,
            brutal_kill,
            worker,
            [zomp_node]},

    Children = [Node],
    {ok, {RestartStrategy, Children}}.
