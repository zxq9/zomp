%%% @doc
%%% Zomp Node and Network Tracker
%%%
%%% This named process keeps track of the connection state of other nodes and whether
%%% all configured realms are available.
%%%
%%%
%%% The connection process
%%%
%%% When told to establish connections this process first attempts to connect to the
%%% prime node for each realm that is configured. As realm advertisements, redirect
%%% responses and connection failures are recieved the node connectors are guided to
%%% eventually settle on a connection scheme where duplicate connections are removed
%%% and all configured realms are connected to an upstream node.
%%%
%%% Nodes will only accept 16 downstream node clients at a time before instructing new
%%% connecting nodes to redirect to a lower tier. (The limit for client connections is
%%% 256 before instructing a redirect, with the exception that nodes with no downstream
%%% will never redirect a connecting client.)
%%%
%%% Redirect instructions consist of the redirect token itself and a list of realms
%%% and next-node to try.
%%%
%%% In the event of a connection failure the node connector will attempt to reconnect
%%% to the prime node of whatever unsatisfied realms are configured. If a connection to
%%% a realm prime cannot be established or fails the node will try to reconnect on a
%%% 1-minute interval.
%%%
%%% This module is organized into the following sections:
%%% - Type Definitions
%%% - Service Interface
%%% - Startup
%%% - gen_server
%%% - Doer Functions
%%% - Data Manipulators
%%% @end

-module(zomp_node_man).
-vsn("0.6.3").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([lookup/1]).
-export([connect/0, report/1]).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-export_type([id/0]).


-include("$zx_include/zx_logger.hrl").


%%% Type definitions


-record(s,
        {upstream = []    :: [realm()],
         conns    = []    :: [conn()],
         hosts    = []    :: [zx:host()],
         host_q   = []    :: [zx:host()],
         timeout  = 5     :: integer(),
         ep       = 11311 :: inet:port_number()}).

-record(realm,
        {name      = none :: none | zx:realm(),
         assigned  = none :: none | pid(),
         local     = none :: none | pid(),
         serial    = 0    :: non_neg_integer(),
         available = []   :: [pid()]}).

-record(conn,
        {pid    = none :: none | pid(),
         mon    = none :: none | reference(),
         host   = none :: none | zx:host()}).

-type state()       :: #s{}.
-type realm()       :: #realm{}.
-type conn()        :: #conn{}.
-type id()          :: integer().
-type conn_report() :: {connected, Realms :: [{zx:realm(), zx:serial()}]}
                     | {redirect,  Hosts  :: [zx:host()]}.



%%% Service Interface

-spec lookup(Realm) -> Result
    when Realm  :: zx:realm(),
         Result :: {ok, Node :: pid()}
                 | wait
                 | error.

lookup(Realm) ->
    gen_server:call(?MODULE, {lookup, Realm}).



%%% Upstream Conn Interface

-spec connect() -> ok.

connect() ->
    gen_server:cast(?MODULE, connect).


-spec report(conn_report()) -> ok.
%% @doc
%% Report the outcome or status of an upstream connection

report({Action, Data}) ->
    gen_server:cast(?MODULE, {report, Action, self(), Data}).



%%% Startup

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.
%% @private
%% Startup function -- initially called by supervisor.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
%% @private
%% gen_server callback for startup

init(none) ->
    {ok, Timeout} = zx_daemon:conf(timeout),
    {ok, Realms}  = zx_daemon:conf(realms),
    {ok, Managed} = zx_daemon:conf(managed),
    Unmanaged = lists:subtract(Realms, Managed),
    Upstream = lists:map(fun populate/1, Unmanaged),
    Hosts =
        case zx_daemon:hosts() of
            {ok, Hs} -> Hs;
            _        -> []
        end,
    State = #s{upstream = Upstream,
               timeout  = Timeout,
               hosts    = Hosts},
    {ok, State}.


populate(Realm) ->
    {ok, Pid} = zomp_realm_man:lookup(Realm),
    {ok, Serial} = zomp_realm:serial(Pid),
    #realm{name = Realm, local = Pid, serial = Serial}.


%%% gen_server

handle_call({lookup, Realm}, _, State) ->
    {Result, NewState} = do_lookup(Realm, State),
    {reply, Result, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call ~200tp: ~200tp", [From, Unexpected]),
    {noreply, State}.


handle_cast(connect, State) ->
    NewState = do_connect(State),
    {noreply, NewState};
handle_cast({report, connected, Pid, Available}, State) ->
    log(info, "~200tp Connected with Available: ~200tp", [Pid, Available]),
    NewState = ensure_connections(maybe_enroll(Pid, Available, State)),
    {noreply, NewState};
handle_cast({report, redirect, Pid, Hosts}, State) ->
    NewState = ensure_connections(redirect(Pid, Hosts, State)),
    {noreply, NewState};
handle_cast({report, enrolled, Pid, {Realm, Serial}}, State) ->
    NewState = accept_enrollment(Realm, Serial, Pid, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~200tp", [Unexpected]),
    {noreply, State}.


handle_info(reconnect, State) ->
    NewState = ensure_connections(State),
    {noreply, NewState};
handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = ensure_connections(handle_down(Mon, Pid, Reason, State)),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tw", [Unexpected]),
    {noreply, State}.


code_change(_, State, _) ->
    {ok, State}.


terminate(_, _) ->
    ok.



%%% Doer Functions

-spec do_lookup(Realm, State) -> {Result, NewState}
    when Realm    :: zx:realm(),
         State    :: state(),
         Result   :: {ok, pid()}
                   | wait
                   | error,
         NewState :: state().

do_lookup(Realm, State = #s{upstream = Upstream}) ->
    case lists:keyfind(Realm, #realm.name, Upstream) of
        R = #realm{assigned = none, available = [Pid | Rest]} ->
            ok = zomp_node:enroll(Pid, Realm),
            NewR = R#realm{assigned = Pid, available = Rest},
            NewUpstream = lists:keyreplace(Realm, #realm.name, Upstream, NewR),
            {{ok, Pid}, State#s{upstream = NewUpstream}};
        #realm{assigned = none, available = []} ->
            {wait, State};
        #realm{assigned = Pid} ->
            {{ok, Pid}, State};
        false ->
            {error, State}
    end.


-spec do_connect(State) -> NewState
    when State    :: state(),
         NewState :: state().

do_connect(State = #s{upstream = Upstream, conns = Conns, hosts = Hosts, ep = EP}) ->
    Realms = [Name || #realm{name = Name} <- Upstream],
    Connectorate = initiator(Realms, EP),
    NewConns = lists:foldl(Connectorate, Conns, Hosts),
    State#s{conns = NewConns, host_q = []}.


initiator(Realms, EP) ->
    fun(Host, Conns) ->
        {ok, Pid} = zomp_node:start(Host, Realms, EP),
        Mon = monitor(process, Pid),
        [#conn{pid = Pid, mon = Mon, host = Host} | Conns]
    end.


ensure_connections(State = #s{upstream = Upstream,
                              conns    = Conns,
                              host_q   = [Host | Hosts],
                              ep       = EP}) ->
    case lists:keymember(none, #realm.assigned, Upstream) of
        true ->
            Realms = [Name || #realm{name = Name} <- Upstream],
            {ok, Pid} = zomp_node:start(Host, Realms, EP),
            Mon = monitor(process, Pid),
            NewConns = [#conn{pid = Pid, mon = Mon, host = Host} | Conns],
            State#s{conns = NewConns, host_q = Hosts};
        false ->
            State
    end;
ensure_connections(State = #s{hosts = Hosts, host_q = []}) ->
    ok = log(info, "Host list exhausted. Retrying in 5 seconds..."),
    _ = erlang:send_after(5000, self(), reconnect),
    State#s{host_q = Hosts}.


maybe_enroll(Pid, Available, State = #s{upstream = Upstream, conns = Conns}) ->
    log(info, "Maybe enrolling..."),
    case maybe_assign(unassigned, Pid, Available, Upstream) of
        unassigned ->
            {value, #conn{mon = Mon}, NewConns} = lists:keytake(Pid, #conn.pid, Conns),
            true = demonitor(Mon, [flush]),
            ok = zomp_node:stop(Pid),
            State#s{conns = NewConns};
        {assigned, NewUpstream} ->
            State#s{upstream = NewUpstream}
    end.


maybe_assign(Outcome, Pid, [{Realm, CS} | Rest], Upstream) ->
    log(info, "Maybe assigning..."),
    {NewOutcome, NewUpstream} =
        case lists:keytake(Realm, #realm.name, Upstream) of
            {value, R = #realm{assigned = none, serial = OS}, NU} when CS >= OS ->
                ok = zomp_node:enroll(Pid, Realm),
                {assigned, [R#realm{assigned = Pid, serial = CS} | NU]};
            {value, R = #realm{assigned = Old, serial = OS}, NU} when CS > OS ->
                ok = zomp_node:stop(Old),
                ok = zomp_node:enroll(Pid, Realm),
                {assigned, [R#realm{assigned = Pid, serial = CS} | NU]};
            {value, R = #realm{serial = CS, available = Available}, NU} ->
                {Outcome, [R#realm{available = [Pid | Available]} | NU]};
            _ ->
                {Outcome, Upstream}
        end,
    maybe_assign(NewOutcome, Pid, Rest, NewUpstream);
maybe_assign(assigned, _, [], Upstream) ->
    {assigned, Upstream};
maybe_assign(unassigned, _, [], _) ->
    unassigned.


accept_enrollment(Realm, Serial, Node, State = #s{upstream = Upstream}) ->
    log(info, "~tp enrolling to ~tp:~tp, State: ~200tp", [Node, Realm, Serial, State]),
    case lists:keytake(Realm, #realm.name, Upstream) of
        {value, #realm{local = none}, _} ->
            State;
        {value, #realm{serial = Serial, local = Local, assigned = Node}, _} ->
            ok = zomp_realm:upstream(Local, Node),
            ok = zomp_realm:serial(Local, Serial),
            State;
        {value, R = #realm{serial = S, local = Local}, NextUS} when S =< Serial ->
            ok = zomp_realm:upstream(Local, Node),
            ok = zomp_realm:serial(Local, Serial),
            NewR = R#realm{serial = Serial, assigned = Node},
            State#s{upstream = [NewR | NextUS]};
        false ->
            ok = zomp_node:stop(Node),
            State
    end.


redirect(Pid,
         Suggested,
         State = #s{upstream = Upstream, conns = Conns, hosts = Hosts}) ->
    NewUpstream =
        case lists:keytake(Pid, #realm.assigned, Upstream) of
            {value, R = #realm{available = []}, NextUpstream} ->
                [R#realm{assigned = none} | NextUpstream];
            {value, R = #realm{name = Name, available = [Next | Rest]}, NextUpstream} ->
                ok = zomp_node:enroll(Next, Name),
                [R#realm{assigned = Next, available = Rest} | NextUpstream];
            false ->
                Upstream
        end,
    {value, #conn{mon = Mon}, NewConns} = lists:keyake(Pid, #conn.pid, Conns),
    true = demonitor(Mon, [flush]),
    NewHosts = Suggested ++ Hosts,
    State#s{upstream = NewUpstream, conns = NewConns, hosts = NewHosts}.


-spec handle_down(Mon, Pid, Info, State) -> NewState
    when Mon      :: reference(),
         Pid      :: pid(),
         Info     :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% Stop tracking closed connection.

handle_down(Mon, Pid, Info, State = #s{upstream = Upstream, conns = Conns}) ->
    NewConns =
        case lists:keytake(Mon, #conn.mon, Conns) of
            {value, #conn{host = Host}, NextConns} ->
                HostString = zx_net:host_string(Host),
                ok = log(info, "~ts went down with ~200tp", [HostString, Info]),
                NextConns;
            false ->
                Unexpected = {'DOWN', Mon, process, Pid, Info},
                ok = log(warning, "Unexpected info: ~w", [Unexpected]),
                Conns
        end,
    NewUpstream = scrub_upstream(Pid, Upstream, []),
    State#s{upstream = NewUpstream, conns = NewConns}.


scrub_upstream(Pid,
               [R = #realm{assigned = Pid, available = []} | Rs],
               Acc) ->
    scrub_upstream(Pid, Rs, [R#realm{assigned = none} | Acc]);
scrub_upstream(Pid,
               [R = #realm{name = Name, assigned = Pid, available = [A | As]} | Rs],
               Acc) ->
    ok = zomp_node:enroll(A, Name),
    scrub_upstream(Pid, Rs, [R#realm{assigned = A, available = As} | Acc]);
scrub_upstream(Pid,
               [R = #realm{available = As} | Rs],
               Acc) ->
    NewAs = lists:delete(Pid, As),
    scrub_upstream(Pid, Rs, [R#realm{available = NewAs} | Acc]);
scrub_upstream(_, [], Acc) ->
    Acc.
