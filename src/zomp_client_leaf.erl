%%% @doc
%%% Zomp LEAF Client
%%% 
%%% This module implements anonymous user utility connection functions.
%%%
%%% Anonymous user connections are persistent to give users executing a client program
%%% a chance to subscribe to version updates of any programs they are running and a
%%% semi-permanent path to Zomp nodes for any other zx-driven tasks are being performed
%%% on their system.
%%%
%%% When subscribed a 60-second timeout-to-ping is implemented that ensures the TCP
%%% connection will be kept alive despite unusual NAT session settings that fail to
%%% respect TCP's actual keepalive tick. When unsubscribed a connection will time out
%%% after 60 seconds of inactivity and be closed. There is no connection upgrade path
%%% from an anonymous user mode connection and an authenticated session -- auth
%%% requests, while potentially involving an artbitrary amount of protocol-defined
%%% synchronous communication, are not persistent.
%%%
%%% A complete list of messages are defined by the typedef for `incoming()'. The effect
%%% and handler implementations are found in the dispatch/2 function and Handlers
%%% section of this module.
%%%
%%% This module is organized into the following sections:
%%% - Constants, Definitions, and Types
%%% - Interface
%%% - Service Loop
%%% - Message Dispatch
%%% - Handlers
%%% - Harness Checkers
%%% - OTP Conformance Functions
%%% @end

-module(zomp_client_leaf).
-vsn("0.6.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([start_session/4]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).

-include("$zx_include/zx_logger.hrl").


%%% Constants, Definitions, and Types


-record(s, {socket = none       :: none | gen_tcp:socket(),
            timer  = none       :: none | reference(),
            realms = #{}        :: #{zx:realm() := pid()},
            subs   = sets:new() :: subs()}).

-type state() :: #s{}.

% TODO: This is still unused. A zomp_realm crash will silently drop subscriptions.
-type subs()  :: sets:set(zx:package()).


%%% Session Init

-spec start_session(Parent, Debug, Socket, Bin) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         Socket :: gen_tcp:socket(),
         Bin    :: binary().

start_session(Parent, Debug, Socket, Bin) ->
    case zx_lib:b_to_ts(Bin) of
        {ok, Realms} ->
            start_session2(Parent, Debug, Socket, Realms);
        error ->
            ok = gen_tcp:send(Socket, zx_net:err_ex(bad_message)),
            ok = zx_net:disconnect(Socket),
            terminate()
    end.

start_session2(Parent, Debug, Socket, Realms) when is_list(Realms) ->
    case lists:all(fun zx_lib:valid_lower0_9/1, Realms) of
        true ->
            start_session3(Parent, Debug, Socket, Realms);
        false ->
            ok = gen_tcp:send(Socket, zx_net:err_ex(bad_message)),
            ok = zx_net:disconnect(Socket),
            terminate()
    end;
start_session2(_, _, Socket, _) ->
    ok = gen_tcp:send(Socket, zx_net:err_ex(bad_message)),
    ok = zx_net:disconnect(Socket),
    terminate().


start_session3(Parent, Debug, Socket, Realms) ->
    {ok, Host} = zx_net:peername(Socket),
    case zomp_client_man:connected(leaf, Host, Realms) of
        {accept, Available} ->
            {ReportBin, Local} = unpack(Available, [], #{}),
            ok = gen_tcp:send(Socket, <<0:8, ReportBin/binary>>),
            Timer = erlang:send_after(60000, self(), ping),
            State = #s{socket = Socket, timer = Timer, realms = Local},
            loop(Parent, Debug, State);
        {redirect, Nodes} ->
            NodesBin = term_to_binary(Nodes),
            ok = gen_tcp:send(Socket, <<1:8, NodesBin/binary>>),
            ok = zx_net:disconnect(Socket),
            terminate()
    end.

unpack([{Realm, Serial, Pid} | Rest], Report, Local) ->
    unpack(Rest, [{Realm, Serial} | Report], maps:put(Realm, Pid, Local));
unpack([], Report, Local) ->
    {term_to_binary(Report), Local}.



%%% Service Loop

-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% Main service loop.

loop(Parent, Debug, State = #s{socket = Socket, timer = Timer}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<0:1, Code:7, Reference/binary>>} ->
            _ = erlang:cancel_timer(Timer),
            {ok, Target} = zx_lib:b_to_ts(Reference),
            ok = handle(Code, Target, State),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        ping ->
            ok = ping(Socket),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        {notify, PackageID} ->
            _ = erlang:cancel_timer(Timer),
            Message = <<1:1, 1:7, (term_to_binary(PackageID))/binary>>,
            ok = gen_tcp:send(Socket, Message),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        {update, Realm, Serial} ->
            _ = erlang:cancel_timer(Timer),
            Message = <<1:1, 2:7, (term_to_binary({Realm, Serial}))/binary>>,
            ok = gen_tcp:send(Socket, Message),
            NewTimer = erlang:send_after(60000, self(), ping),
            loop(Parent, Debug, State#s{timer = NewTimer});
        {tcp_closed, Socket} ->
            _ = erlang:cancel_timer(Timer),
            ok = log(info, "Socket closed, retiring."),
            terminate();
        disconnect ->
            _ = erlang:cancel_timer(Timer),
            ok = log(info, "'disconnect' received. Closing socket and retiring."),
            ok = zomp_net:disconnect(Socket),
            terminate();
        {system, From, Request} ->
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State);
        Unexpected ->
            ok = log(warning, "Unexpected message: ~tw", [Unexpected]),
            loop(Parent, Debug, State)
    end.


-spec ping(gen_tcp:socket()) -> ok | no_return().

ping(Socket) ->
    {ok, {IP, Port}} = inet:peername(Socket),
    Addr = inet:ntoa(IP),
    ok = io:format("~p: Pinging LEAF ~ts:~w~n", [self(), Addr, Port]),
    ok = gen_tcp:send(Socket, <<1:1, 0:7>>),
    receive
        {tcp, Socket, <<1:1, 0:7>>} ->
            tell("~ts:~w: Pong!", [Addr, Port]);
        {tcp_closed, Socket} ->
            exit(normal)
        after 5000 ->
            ok = zx_net:disconnect(Socket),
            exit(normal)
    end.


-spec handle(Code, Target, State) -> ok
    when Code   :: 1..10,
         Target :: zx:realm()
                 | zx:package()
                 | zx:package_id()
                 | zx:key_id()
                 | zx:package_type(),
         State  :: state().

handle(Code, Target, State = #s{socket = Socket}) ->
    case interpret(Code, Target, State) of
        noreply  -> ok;
        Response -> gen_tcp:send(Socket, Response)
    end.


-spec interpret(Code, Target, State) -> Result
    when Code   :: 1..10,
         Target :: zx:realm()
                 | zx:package()
                 | zx:package_id()
                 | zx:key_id()
                 | zx:package_type(),
         State  :: state(),
         Result :: noreply | iodata().

interpret(Code, {R, N, V}, State) -> interpret(Code, R, {N, V}, State);
interpret(Code, {R, T}, State)    -> interpret(Code, R, T, State);
interpret(Code, R, State)         -> interpret(Code, R, none, State).


interpret(Code, Realm, Target, #s{socket = Socket, realms = Realms}) ->
    ok = log(info, "Code: ~200p, Realm: ~s, Target: ~200tp", [Code, Realm, Target]),
    Result =
        case maps:find(Realm, Realms) of
            {ok, Pid} -> dispatch(Code, Pid, Realm, Target, Socket);
            error     -> {error, bad_realm}
        end,
    exterpret(Result).


dispatch(Code, Pid, Realm, Target, Socket) ->
    case Code of
         1 -> zomp_realm:subscribe(Pid, Target);
         2 -> zomp_realm:unsubscribe(Pid, Target);
         3 -> zomp_realm:list(Pid);
         4 -> zomp_realm:list(Pid, Target);
         5 -> zomp_realm:latest(Pid, Target);
         6 -> zomp_realm:describe(Pid, Target);
         7 -> zomp_realm:tags(Pid, Target);
         8 -> zomp_realm:provides(Pid, Target);
         9 -> zomp_realm:search(Pid, Target);
        10 -> zomp_realm:list_deps(Pid, Target);
        11 -> zomp_realm:list_sysops(Pid);
        12 -> zomp_client:fetch(Pid, Realm, Target, Socket);
        13 -> zomp_realm:keychain(Pid, Target);
        14 -> zomp_realm:list_type(Pid, Target);
        _  -> {error, bad_message}
    end.


exterpret(ok)              -> <<0:1, 0:7>>;
exterpret({ok, Response})  -> <<0:1, 0:7, (term_to_binary(Response))/binary>>;
exterpret({error, Reason}) -> zx_net:err_ex(Reason);
exterpret(done)            -> noreply.


-spec terminate() -> no_return().
%% @private
%% Convenience wrapper around the suicide call.
%% In the case that a more formal retirement procedure is required, consider notifying
%% the supervisor with `supervisor:terminate_child(zomp_client_sup, PID)' and writing
%% a proper system_terminate/2.

terminate() ->
    exit(normal).


%%% OTP Conformance Functions

-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().

system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).


-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).


-spec system_get_state(term()) -> {ok, term()}.

system_get_state(Misc) -> {ok, Misc}.


-spec system_replace_state(fun(), term()) -> {ok, term(), term()}.

system_replace_state(StateFun, Misc) ->
    {ok, StateFun(Misc), Misc}.
