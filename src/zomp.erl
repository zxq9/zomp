%%% @doc
%%% Zomp: A distributed source package repository system for non-telecommies.
%%%
%%% This is the main Zomp interface module. Node operator functions and project-wide
%%% type definitions reside here. Note that because Zomp is intimately linked to ZX
%%% and depends on it for launch, most of the base types that apply to Zomp/ZX as a
%%% system are defined in the zx module, not here (so a package_id() type for Zomp is
%%% a zx:package_id(), not a zomp:package_id() -- which is slightly counterintuitive
%%% but prevents a lot of code duplication).
%%%
%%% Custom local and public service ports can be altered by editing the file at
%%% `~/.zomp/etc/otpr/zomp/node.conf' with the contents:
%%% ```
%%% {public_port, ExternalPortNumber}.
%%% {listen_port, InternalPortNumber}.
%%% '''
%%% Where the *PortNumber variables are replaced with valid port numbers.
%%%
%%% This module is organized into the following sections:
%%% - Type Definitions
%%% - Service Interface
%%% - Startup
%%% @end

-module(zomp).
-vsn("0.6.3").
-behavior(application).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0").

-export([listen/0, ignore/0]).
-export([start/2, stop/0, stop/1]).

-export_type([requestor/0, request_id/0]).

-include("$zx_include/zx_logger.hrl").


%%% Type Definitions

-type requestor()  :: {pid(), request_id()}.
-type request_id() :: pos_integer().


%%% Service Interface

-spec listen() -> Result
    when Result :: ok
                 | {error, Reason},
         Reason :: {listening, inet:port_number()}
                 | term().
%% @doc
%% Instructs the server to listen on the configured port number.

listen() ->
    zomp_client_man:listen().


-spec ignore() -> ok.
%% @doc
%% Tells zomp to stop listening. Does not terminate ongoing connections.

ignore() ->
    zomp_client_man:ignore().



%%% Startup

-spec start(normal, term()) -> {ok, pid()}.
%% @doc
%% Start the application. (application behavior callback)
%%
%% The template below should normally not be called directly, but if you have
%% special needs it may need to be adjusted (and possibly the first argument
%% changed to accept other valid application start/2 arguments.

start(normal, []) ->
    ok = file:set_cwd(zx_lib:zomp_dir()),
    ok = log(info, "Starting Zomp"),
    {ok, Pid} = zomp_sup:start_link(),
    ok = zx_daemon:zomp_mode(),
    ok = zomp_node_man:connect(),
    {ok, Pid}.


-spec stop() -> ok | {error, term()}.
%% @doc
%% Stop the application.

stop() ->
    application:stop(zomp).


-spec stop(term()) -> ok.
%% @doc
%% Stop the application. (application behavior callback)

stop(_) ->
    ok.
